<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "query".
 *
 * @property int $id
 * @property int $user_id
 * @property string $query
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Mesh[] $meshes
 * @property User $user
 */
class Query extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'query';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'query'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['query'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'query' => 'Query',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeshes()
    {
        return $this->hasMany(Mesh::className(), ['query_id' => 'id']);
    }

    public function getCountMeshes()
    {
        return $mashes = Mesh::find()->where(['query_id' => $this->id])->count();

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
