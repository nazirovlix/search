<?php

namespace app\models;

use function PHPSTORM_META\type;
use SimpleXMLElement;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii2tech\csvgrid\CsvGrid;

/**
 * @param object $xmlObject
 *
 * @return array
 * @access public
 */
class BaseModel extends Model
{
    const AllCount = 29000000;

    /**
     * @param $xmlObject
     * @return array
     */
    function simpleXmlToArray($xmlObject)
    {
        $array = [];
        if (!empty($xmlObject)) {
            foreach ($xmlObject->children() as $node) {
                $array[$node->getName()] = is_array($node) ? simplexml_to_array($node) : (string)$node;
            }
        }
        return $array;

    }

    /**
     * @param $xmlObject
     * @param array $out
     * @return array
     */
    function xml2array($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;

        return $out;
    }

    /**
     * @param $articles
     * @return array
     */
    public function getArray($articles)
    {
        $array = [];

        if (!empty($articles)) {
            foreach ($articles as $article) {
                if (!empty($article->xml[0])) {
                    $title = (string)$article->xml[0]->ArticleTitle[0];
                    $array[] = [
                        'pmid' => $article->pmid,
                        'journal' => !empty((string)$article->xml[0]->Journal->ISOAbbreviation) ? (string)$article->xml[0]->Journal->ISOAbbreviation : '',
                        'pii' => !empty((string)$article->xml[0]->ELocationID[0]) ? (string)$article->xml[0]->ELocationID[0] : '',
                        'date' => (string)$article->xml[0]->ArticleDate->Year . '.' . (string)$article->xml[0]->ArticleDate->Month . '.' . (string)$article->xml[0]->ArticleDate->Day,
                        'doi' => !empty((string)$article->xml[0]->ELocationID[1]) ? (string)$article->xml[0]->ELocationID[1] : '',
                        'title' => !empty($title) ? $title : 'Title',
                        'content' => substr((string)$article->xml[0]->Abstract->AbstractText, 0, 2000),
                        'meshs' => self::foreachTwo($article->mesh)
                    ];
                }

            }
        }
        return $array;
    }

    /**
     * @param $array
     * @return array
     */
    public function foreachTwo($array)
    {
        $meshs = [];
        if (!empty($array->MeshHeading)) {
            for ($k = 0; $k < count($array->MeshHeading); $k++) {
                $meshs[] = (string)$array->MeshHeading[$k]->DescriptorName;
            }
        }

        return $meshs;
    }

    /**
     * @param $session
     * @return array
     */
    public function result($session)
    {
        $response = [];
        if (!empty($session)) {
            foreach ($session as $article) {
                foreach ($article['meshs'] as $mesh) {
                    if (isset($response[$mesh])) {
                        $response[$mesh] = $response[$mesh] . ', <a href="http://ncbi.nlm.nih.gov/pubmed/' . $article['pmid'] . '">' . $article['pmid'] . '</a>';
                    } else {
                        $response[$mesh] = '<a href="http://ncbi.nlm.nih.gov/pubmed/' . $article['pmid'] . '">' . $article['pmid'] . '</a>';
                    }
                }
            }
        }
        $res = [];
        if (!empty($response)) {
            foreach ($response as $mesh => $item) {
                $res[] = [
                    'mesh' => $mesh,
                    'pmids' => $item
                ];
            }
        }
        if (!empty($res) && isset($res)) {
            $res = self::sortQty($res);
        }
        return $res;
    }

//    Without a href
    public function resultWithoutA($session)
    {
        $response = [];
        if (!empty($session)) {
            foreach ($session as $article) {
                foreach ($article['meshs'] as $mesh) {
                    if (isset($response[$mesh])) {
                        $response[$mesh] = $response[$mesh] . ',' . $article['pmid'];
                    } else {
                        $response[$mesh] = $article['pmid'];
                    }
                }
            }
        }
        $res = [];
        if (!empty($response)) {
            foreach ($response as $mesh => $item) {
                $res[] = [
                    'mesh' => $mesh,
                    'pmids' => $item
                ];
            }
        }
        if (!empty($res) && isset($res)) {
            $res = self::sortQty($res);
        }
        return $res;
    }

//      Destroy Sessions
    public function destroy()
    {
        \Yii::$app->session->open();
        $session = \Yii::$app->session;
        return $session->destroy();
    }

    public function pagination($req, $allArticles)
    {
        $page = !empty($req['page']) ? (int)$req['page'] : 1;
        $total = count($allArticles);
        $limit = !empty($req['limit']) ? (int)$req['limit'] : 100; //per page
        $totalPages = ceil($total / $limit); //calculate total pages
        $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
        $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
        $offset = ($page - 1) * $limit;
        if ($offset < 0) $offset = 0;

        return [
            'page' => $page,
            'limit' => $limit,
            'offset' => $offset,
            'totalPages' => $totalPages,
            'total' => $total
        ];

    }

    // По убыванию: QTY
    public function sortQty($result)
    {
        $a = $result;
        uasort($a, function ($a, $b) {
            $countA = substr_count($a['pmids'], ',');
            $countB = substr_count($b['pmids'], ',');
            return ($countA < $countB);
        });
        return $a;
    }

//    Export Query
    public function exportCSV($query, $pmid, $abst)
    {

        $search = \Yii::$app->session['search'] ? \Yii::$app->session['search'] : '';
        $name = $search . '-' . time();
        $info = [];
        if (!empty($query)) {
            foreach ($query as $k => $article) {
                $info[] = [
                    'PMID' => $article['pmid'],
                    'Title' => $article['title'],
                    'Date' => $article['date'] ? $article['date'] : ' ',
                    'Content' => $article['content']
                ];
            }
        }
        $columns = [];
        if ($pmid == 'true' && $abst == 'true') {
            $columns = [
                ['attribute' => 'PMID',],
                ['attribute' => 'Title',],
                ['attribute' => 'Date',],
                ['attribute' => 'Content',]
            ];
        } elseif ($pmid == 'true') {
            $columns = [
                [
                    'attribute' => 'PMID',
                ],
            ];
        } elseif ($abst == 'true') {
            $columns = [
                [
                    'attribute' => 'Title',
                ],
                [
                    'attribute' => 'Date',
                ],
                [
                    'attribute' => 'Content',
                ],
            ];
        }
        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $info
            ]),
            'resultConfig' => [
                'forceArchive' => true // always archive the results
            ],
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'cellDelimiter' => ';',
            ],
            'columns' => $columns,
        ]);

        $export = $exporter->export()->saveAs('uploads/' . $name . '.zip');
        if ($export) {
            return $name;
        }
        return false;
    }

//  Export Result
    public function exportResultCSV($result, $pmid, $meshk)
    {
        $search = \Yii::$app->session['search'] ? \Yii::$app->session['search'] : '';
        $name = $search . '-' . time();
        $info = [];
        if (!empty($result)) {
            foreach ($result as $k => $article) {
                $count = substr_count($article['pmids'], ',');
                $count++;
                $info[] = [
                    'Mesh' => $article['mesh'],
                    'QTY' => $count . ';',
                    'PMIDs' => $article['pmids']
                ];
            }
        }
        $columns = [];
        if ($pmid == 'true' && $meshk == 'true') {
            $columns = [
                [
                    'attribute' => 'Mesh',
                ],
                [
                    'attribute' => 'QTY',
                ],
                [
                    'attribute' => 'PMIDs',
                ]
            ];
        } elseif ($pmid == 'true') {
            $columns = [

                [
                    'attribute' => 'QTY',
                ],
                [
                    'attribute' => 'PMIDs',
                ]
            ];
        } elseif ($meshk == 'true') {
            $columns = [
                [
                    'attribute' => 'Mesh',
                ],
            ];
        }

        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $info
            ]),
            'resultConfig' => [
                'forceArchive' => true // always archive the results
            ],
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'cellDelimiter' => ';',
            ],
            'columns' => $columns,
        ]);

        $export = $exporter->export()->saveAs('uploads/' . $name . '.zip');
        if ($export) {
            return $name;
        }
        return false;

    }

//    Export to Mesh
    public function resultMesh($allArticles, $pmid, $meshk)
    {
        $search = \Yii::$app->session['search'] ? \Yii::$app->session['search'] : '';
        $name = $search . '-' . time();
        $info = [];
        if (!empty($allArticles)) {
            foreach ($allArticles as $k => $article) {
                $mesh = '';
                foreach ($article['meshs'] as $key => $item) {
                    $mesh .= $item . (count($article['meshs']) != ++$key ? ' | ' : '');
                }

                $info[] = [
                    'PMID' => $article['pmid'],
                    'QTY' => count($article['meshs']),
                    'MESHs' => $mesh . ';'
                ];
            }
        }
        $columns = [];
        if ($pmid == 'true' && $meshk == 'true') {
            $columns = [
                [
                    'attribute' => 'PMID',
                ],
                [
                    'attribute' => 'QTY',
                ],
                [
                    'attribute' => 'MESHs',
                ]
            ];
        } elseif ($pmid == 'true') {
            $columns = [
                [
                    'attribute' => 'PMID',
                ],
            ];
        } elseif ($meshk == 'true') {
            $columns = [
                [
                    'attribute' => 'QTY',
                ],
                [
                    'attribute' => 'MESHs',
                ]
            ];
        }
        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $info
            ]),
            'resultConfig' => [
                'forceArchive' => true // always archive the results
            ],
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'cellDelimiter' => ';',
            ],
            'columns' => $columns,
        ]);

        $export = $exporter->export()->saveAs('uploads/' . $name . '.zip');
        if ($export) {
            return $name;
        }
        return false;
    }

    public function exportTanimoto($allArticles)
    {
        $search = Yii::$app->session['search'] ?: '';
        $name = $search . '-' . time();
        $info = [];
        if (!empty($allArticles)) {
            foreach ($allArticles as $k => $article) {
                if ($article['tanimoto'] > 0) {
                    $info[] = [
                        'MESHs' => $article['meshOne'] . ' -- ' . $article['meshTwo'],
                        'Tanimoto' => $article['tanimoto'],
                        'QTY PMIDs' => count($article['pmids']),
                        'PMIDs' => implode(';', $article['pmids']),
                    ];
                }
            }
        }

        $columns = [
            [
                'attribute' => 'MESHs',
            ],
            [
                'attribute' => 'Tanimoto',
            ],
            [
                'attribute' => 'QTY PMIDs',
            ],
            [
                'attribute' => 'PMIDs',
            ]
        ];

        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $info
            ]),
            'resultConfig' => [
                'forceArchive' => true // always archive the results
            ],
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'cellDelimiter' => ';',
            ],
            'columns' => $columns,
        ]);

        $export = $exporter->export()->saveAs('uploads/' . $name . '.zip');
        if ($export) {
            return $name;
        }
        return false;
    }


    /** 5 Simmilar PMIDS for one PMID
     * @param $pmids
     * @return array
     * @throws Exception
     */
    public function similarList($pmids)
    {
        $curl = curl_init();
        $url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom';
        $url .= "?db=pubmed";
        $url .= "&id=" . $pmids;
        $url .= "&cmd=neighbor_score";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $rs = curl_exec($curl);
        $curl_error = curl_error($curl);
        curl_close($curl);

        if ($curl_error) {
            throw new Exception($curl_error);
        }
        $array[] = $pmids;
        $rs = $this->isXmlStructureValid($rs);
        $return = simplexml_load_string($rs);
        $return = (array)$return->LinkSet->LinkSetDb[2];
        if (!empty($return['Link'])) {
            foreach ($return['Link'] as $item) {
                $a = (array)$item;
                if (!empty($a['Id']))
                    $array[] = $a['Id'];
            }
        }
        return $array;
    }

    public function similarFromPmid($pmid)
    {
        $session = Yii::$app->session;
        if (empty($session['similars'][$pmid])) {
            $pmids = self::similarList($pmid);
            if ($pmids) {
                $similars = $session['similars'];
                $similars[$pmid] = $pmids;
                $session['similars'] = $similars;
            }
        }
        return $session['similars'][$pmid];
    }

    public function querySimilars($withHref = true)
    {
        $session = Yii::$app->session;
        $result = $session['result'];
        foreach ($result as $key => $item) {
            $pmids = explode(',', $item['pmids']);
            foreach ($pmids as $pmid) {
                $result[$key]['similars'][] = self::similarFromPmid(strip_tags($pmid));
            }
            $array = [];
            foreach ($result[$key]['similars'] as $item) {
                foreach ($item as $pmid) {
                    if (!in_array($pmid, $array)) {
                        $array[] = $withHref === true ? '<a href="http://ncbi.nlm.nih.gov/pubmed/' . $pmid . '">' . $pmid . '</a>' : $pmid;
                    }
                }

            }
            unset($result[$key]['similars']);
            unset($result[$key]['pmids']);
            $result[$key]['pmids'] = implode(',', $array);
        }
        return $result;
    }

    // Index Page sort PMIDs
    public function sortPmids($allArticles, $sort)
    {
        $a = $allArticles;
        if ($sort == 'sortDesc') {
            uasort($a, function ($a, $b) {
                $countA = $a['pmid'];
                $countB = $b['pmid'];
                return ($countA < $countB);
            });
        } else {
            uasort($a, function ($a, $b) {
                $countA = $a['pmid'];
                $countB = $b['pmid'];
                return ($countA > $countB);
            });
        }
        return $a;
    }

    // Sort QTY Meshs
    public function sortMesh($allArticles, $sort)
    {
        $a = $allArticles;
        if ($sort == 'sortDesc') {
            uasort($a, function ($a, $b) {
                $countA = count($a['meshs']);
                $countB = count($b['meshs']);
                return ($countA < $countB);
            });
        } else {
            uasort($a, function ($a, $b) {
                $countA = count($a['meshs']);
                $countB = count($b['meshs']);
                return ($countA > $countB);
            });
        }

        return $a;
    }

    // Sort Result
    public function sortResult($allArticles, $sort)
    {
        $a = $allArticles;
        if ($sort == 'sortDesc') {
            uasort($a, function ($a, $b) {
                $countA = substr_count($a['pmids'], ',');
                $countB = substr_count($b['pmids'], ',');
                return ($countA < $countB);
            });
        } else {
            uasort($a, function ($a, $b) {
                $countA = substr_count($a['pmids'], ',');
                $countB = substr_count($b['pmids'], ',');
                return ($countA > $countB);
            });
        }

        return $a;
    }

    // reuslt Mesh
    public function sortResultMesh($result, $sort)
    {
        $a = $result;
        if ($sort == 'sortDesc') {
            uasort($a, function ($a, $b) {
                return strcmp($a['mesh'], $b['mesh']);
            });
        } else {
            uasort($a, function ($a, $b) {
                return strcmp($b['mesh'], $a['mesh']);
            });
        }

        return $a;

    }

    // Third stage | sort Feqs
    public function sortResultFeq($result, $sortFeq)
    {
        $a = $result;
        if ($sortFeq == 'sortDesc') {
            uasort($a, function ($a, $b) {
                $countA = $a['count'];
                $countB = $b['count'];
                return ($countA < $countB);
            });
        } else {
            uasort($a, function ($a, $b) {
                $countA = $a['count'];
                $countB = $b['count'];
                return ($countA > $countB);
            });
        }
        return $a;
    }

    /** Count of million.txt
     * @param $count
     * @return SimpleXMLElement
     */
    public function millionCount()
    {
        $session = Yii::$app->session;

        $million = Pmids::findOne(1);
        $million = Url::base('http') .'/uploads/' . $million->file;
        $count = $session['amount'] ?: $session['count'];
        $file = !empty($million) ? file($million) : [];
        $arr = array_rand($file, $count);
        $pmids = [];
        if(is_array($arr)){
            foreach ($arr as $item) {
                $pmids[] = (int)$file[$item];
            }
        } else {
            $pmids[] = (int)$file[$arr];
        }
        $newPmids = array_chunk($pmids, 100);

        $xml = '';
        if (count($newPmids) > 0) {
            for ($i = 0; $i < count($newPmids); $i++) {
                $xml = $this->millionCountReq($newPmids[$i], $xml);
            }
        } else {
            $xml = $this->millionCountReq($newPmids[0], $xml);
        }

        return $xml;

    }

    public function millionCountReq($pmid, $xml)
    {
        $pmid = implode(",", $pmid);
        $curl = curl_init();
        $url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi';
        $url .= "?db=pubmed";
        $url .= "&id=" . $pmid;
        $url .= "&retmode=xml";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $rs = curl_exec($curl);
        $curl_error = curl_error($curl);
        curl_close($curl);
        if ($curl_error) {
            throw new Exception($curl_error);
        }


        if ($xml === '') {
            $rs = $this->isXmlStructureValid($rs);
            if ($rs)
                $xml = simplexml_load_string($rs);
        } else {
            $rs = $this->isXmlStructureValid($rs);
            if ($rs) {
                $xml2 = simplexml_load_string($rs);
                $old = (array)$xml->PubmedArticle;
                $new = (array)$xml2->PubmedArticle;
                array_push($old, $new);
            }
        }
        return $xml;
    }

    /** Check is has mesh in PMID | humans true
     * @param $pmid
     * @param $mesh
     * @return bool
     * @throws Exception
     */
    public function checkPmid($xml, $mesh)
    {
        if (!empty($xml)) {
            $count = 0;
            foreach ($xml as $article) {
                if (!empty($article->MedlineCitation->MeshHeadingList)) {
                    $xml = $article->MedlineCitation->MeshHeadingList;
                    $meshs = $this->foreachTwo($xml);
                    if (in_array($mesh, $meshs)) {
                        $count++;
                    }
                }
            }
            return $count;
        }

        return false;
    }

    /** All count on Pubmed from Mesh | Humans = 17539211
     * @param $meshs
     * @return array
     * @throws Exception
     */
    public function addMeshCounts($meshs)
    {
        $meshs = array_values($meshs);
        $k = 1;
        $newMesh = [];
        foreach ($meshs as $item) {
            $item = str_replace(' ', '+', $item['mesh']);
            $item = '"' . $item . '"%5BAll+Fields%5D';
            if ($k === count($meshs) || ($k % 50 === 0)) {
                $newMesh[] = $item;
            } else {
                $newMesh[] = $item . '+and+';
            }
            $k++;
        }
        $newMesh = array_chunk($newMesh, 50);
        $array = [];
        if (count($newMesh) > 0) {
            for ($i = 0; $i < count($newMesh); $i++) {
                $array = $this->getTermCount($newMesh[$i], $array);
            }
        } else {
            $array = $this->getTermCount($newMesh[0], $array);
        }


        foreach ($meshs as $key => $mesh) {
            foreach ($array as $item) {
                $text = '"' . $mesh['mesh'] . '"[All Fields]';
                if (strtolower($item['Term']) == strtolower($text)) {
                    $pubmedCount = $item['Count'] ?: 1;
                    $meshs[$key]['count'] = ($pubmedCount * 100) / 29000000;
                }
            }
        }
        return $meshs;
    }

    /**
     * @param $term
     * @param array $array
     * @return array
     * @throws Exception
     */
    public function getTermCount($term, $array = [])
    {


        $curl = curl_init();
        $url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi';
        $url .= "?db=pubmed";
        $url .= "&term=" . implode('', $term);
        $url .= "&usehistory=xml";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        $rs = curl_exec($curl);
        $curl_error = curl_error($curl);
        curl_close($curl);
        if ($curl_error) {
            throw new Exception($curl_error);
        }
        $rs = $this->isXmlStructureValid($rs);
        $return = simplexml_load_string($rs);

        $return = (array)$return->TranslationStack;
        if (!empty($return['TermSet'])) {
            foreach ($return['TermSet'] as $item) {
                $item = (array)$item;
                if (!empty($item['Field']) && ($item['Field'] == 'All Fields')) {
                    $array[] = $item;
                }
            }
        }
        return $array;
    }


    /**
     * Tonimoto koefitsent
     * @param $array1
     * @param $array2
     * @return int
     */
    public function getTonimoto($array1, $array2)
    {
        $ton = (count(array_intersect($array1, $array2))) / (count($array1) + count($array2) - count(array_intersect($array1, $array2)));
        return $ton ?: 0;
    }

    public function getCoefTonimoto($allArt)
    {
        $count = count($allArt);
        $arr = [];
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = $i + 1; $j < $count; $j++) {
                $pmid = explode(',', $allArt[$i]['pmids']);
                $pmid2 = explode(',', $allArt[$j]['pmids']);
                $arr[$i][$j] = $this->getTonimoto($pmid, $pmid2);
            }
        }

        return $arr;

    }

    public function arrayTanimoto($allArt, $tan = null)
    {
        $count = count($allArt);
        $tan = !empty($tan) ? $tan : 0;
        $arr = [];
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = $i + 1; $j < $count; $j++) {
                $pmid = explode(',', $allArt[$i]['pmids']);
                $pmid2 = explode(',', $allArt[$j]['pmids']);
                $tanimoto = $this->getTonimoto($pmid, $pmid2);
                if ($tanimoto >= $tan) {
                    $arr[] = [
                        'meshOne' => $allArt[$i]['mesh'],
                        'meshTwo' => $allArt[$j]['mesh'],
                        'tanimoto' => $tanimoto,
                        'pmids' => array_intersect($pmid, $pmid2)
                    ];
                }
            }
        }

        // debug($arr,1);
        uasort($arr, function ($a, $b) {
            $countA = $a['tanimoto'];
            $countB = $b['tanimoto'];
            return ($countA < $countB);
        });
        return $arr;
    }

    public function isXmlStructureValid($file)
    {
        libxml_use_internal_errors(true);

        $doc = simplexml_load_string($file);
        $xml = explode("\n", $file);

        if (!$doc) {
            $errors = libxml_get_errors();
            foreach ($errors as $error) {
                echo display_xml_error($error, $xml);
            }

            libxml_clear_errors();
            return false;
        }
        return $file;
    }

    public function getNodes($allArt, $countTan, $countTanTo)
    {

        $php = [];
        foreach ($allArt as $key => $article) {
            $count = substr_count($article['pmids'], ',') + 1;

            if ($count < 5) {
                $color = "#9c27b0";
            } elseif ($count >= 5 && $count <= 10) {
                $color = "#f44336";
            } elseif ($count >= 10 && $count <= 20) {
                $color = "#00bcd4";
            } elseif ($count >= 20 && $count <= 50) {
                $color = "#4caf50";
            } elseif ($count >= 50 && $count <= 100) {
                $color = "#e91e63";
            } else {
                $color = "#999999";
            }
            $display = $this->checkTanimoto($allArt, $key, $countTan, $countTanTo) === false ? 'none' : 'block';
            $php['nodes'][] = [
                'data' => [
                    "id" => $key,
                    "score" => 20 + ($count * 2),
                    "name" => $article['mesh'] . '(' . $count . ')',
                    "color" => $color,
                    "display" => $display,
                    "tippy" => '<div>' . $article['pmids'] . '</div>'
                ],
            ];
        }
        return $php;

    }

    public function getEdges($allArt, $php, $countTan, $countTanTo)
    {
        foreach ($allArt as $key => $article) {
            if (count($allArt) > 3) {
                $c = count($allArt) - 1;
            } else {
                $c = count($allArt);
            }
            for ($i = 1; $i <= $c; $i++) {
                if (($key + $i) < count($allArt)) {
                    $tan = $this->getTwoTanimoto($allArt, $key, $i);
                    if ($tan > $countTan && $tan <= $countTanTo) {
                        $php['edges'][] = [
                            'data' => [
                                "id" => (100 * $i) + $key,
                                "source" => $key,
                                "target" => $key + $i,
                                "name" => $article['mesh']
                            ],
                        ];
                    }

                }
            }
        }
        return $php;
    }

    public function checkTanimoto($allArt, $key, $countTan, $countTanTo)
    {
        $countTan = !empty($countTan) ? $countTan : 0;
        $countTanTo = !empty($countTanTo) ? $countTanTo : 1;
        $return = false;
        for($i = 1; $i < count($allArt); $i++){
            if($i !== $key) {
                $pmid = explode(',', $allArt[$key]['pmids']);
                $pmid2 = explode(',', $allArt[$i]['pmids']);
                $tan = $this->getTonimoto($pmid, $pmid2);
                if ($tan > $countTan && $tan <= $countTanTo) {
                    $return = true;
                }
            }

        }
        return $return;
    }

    public function getTwoTanimoto($allArt, $key, $i)
    {
        $pmid = explode(',', $allArt[$key]['pmids']);
        $pmid2 = explode(',', $allArt[$key + $i]['pmids']);
        return $this->getTonimoto($pmid, $pmid2);
    }


}