<?php

namespace app\modules\admin\controllers;

use app\models\Pmids;
use app\models\User;
use mdm\admin\models\form\Login;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $users = User::find()
            ->where(['not', ['id' => 1]])
            ->orderBy(['id' => SORT_DESC])
            ->limit(10)
            ->all();
        $count = User::find()
            ->where(['not', ['id' => 1]])
            ->count();

        $model = Pmids::findOne(['id' => 1]);
        if ($model && $model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('Pmids');
            return $this->refresh();
        }

        return $this->render('index', [
            'users' => $users,
            'count' => $count,
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionUsers()
    {
        $query = User::find()
            ->where(['not', ['id' => 1]])
            ->orderBy(['id' => SORT_DESC]);

        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 20, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $users = $query->offset($pages->offset)->limit($pages->limit)->all();


        return $this->render('users', [
            'users' => $users,
            'pages' => $pages
        ]);
    }
}
