<?php
/* @var $this \yii\web\View */

/* @var $content string */

use app\modules\admin\assets\AdminAsset;
use app\modules\admin\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = 'Admin';

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
        </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                        <!-- Logo icon -->

                        <!--End Logo icon -->
                        <span>B.ENGINE</span>
                    </a>
                </div>
                <!-- End Logo -->

            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?= $this->render('sidebar.php') ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">

            <div class="container-fluid">
                <!-- Start Page Content -->
                <?php if ( Yii::$app->controller->id !== 'default' ): ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" id="admin-content">
                                <?= $content; ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <?= $content; ?>
                <?php endif; ?>
            </div>
            <!-- footer -->
            <footer class="footer"> © 2019</a>
            </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>