<!-- Start Page Content -->
<div class="row">
    <div class="col-md-6">
        <?php use yii\widgets\ActiveForm;

        $form = ActiveForm::begin(); ?>
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <h4>PMIDs file: </h4>
                    <?= $form->field($model, 'file')->fileInput()->label(false) ?>
                    <p class="m-b-0">Last updated at: <b><?php if(!empty($model)) { echo date('d.m.Y', $model->updated_at);  } ?> </b></p>
                </div>
                <div class="media-body media-text-right">
                    <br><button class="btn btn-info" type="submit">Upload file</button><br><br>
                    <a href="/uploads/<?= $model->file ?>" download="million" class="btn btn-success">Download last</a>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6">
        <div class="card p-30" style="min-height: 164px">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $count ?></h2>
                    <p class="m-b-0">All Users</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Last 10 Users</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Search count</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($users)) {
                            foreach ($users as $k => $item):?>
                                <tr>
                                    <td><?= ++$k ?></td>
                                    <td><?= $item->id ?></td>
                                    <td><?= $item->email ?></td>
                                    <td><?= $item->username ?></td>
                                    <td><?= count($item->query) ?></td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->