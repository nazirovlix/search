<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 08.01.2019
 * Time: 1:30
 */
?>
<style>
    .pagination-sm>li:first-child>a, .pagination-sm>li:first-child>span {
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .pagination>li>a {
        background: #fafafa;
        color: #666;
    }
    .pagination-sm>li>a, .pagination-sm>li>span {
        padding: 5px 10px;
        font-size: 12px;
        line-height: 1.5;
    }
    .pagination > .active > a, .pagination > .active > a:hover
    {
        background-color: #3af5ea;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Users</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Search count</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($users)) {
                            foreach ($users as $k => $item):?>
                            <tr>
                                <td><?= ++$k ?></td>
                                <td><?= $item->id ?></td>
                                <td><?= $item->email ?></td>
                                <td><?= $item->username ?></td>
                                <td><?= count($item->query) ?></td>
                            </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix" style="margin: 10px 0">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'options' => [
                        'class' => 'pagination pagination-sm no-margin'
                    ],
                    'activePageCssClass' => 'active',
                    'prevPageLabel' => '«',
                    'nextPageLabel' => '»',


                ]);?>
                </div>
            </div>
        </div>
    </div>
</div>