<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\forms\Login;
use app\models\forms\Signup;
use app\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$login = new Login();
$signup = new Signup();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset=utf-8 />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="/images/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- polyfills so the demo works on old browsers, like IE -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/classlist/1.2.20171210/classList.min.js"></script>
    <script src="https://unpkg.com/bluebird@3.5.2/js/browser/bluebird.js"></script>
    <script src="https://unpkg.com/whatwg-fetch@3.0.0/dist/fetch.umd.js"></script>

    <!-- libs used by demo -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.0/lodash.min.js"></script>
    <script src="https://unpkg.com/webcola@3.3.8/WebCola/cola.min.js"></script>
    <script src="https://unpkg.com/popper.js@1.14.4/dist/umd/popper.js"></script>
    <script src="https://unpkg.com/tippy.js@2.6.0/dist/tippy.all.js"></script>

    <!-- cy libs -->
    <script src="/js/cytospace/cytoscape.min.js"></script>
    <script src="/js/cytospace/cytoscape-cola.js"></script>
    <script src="/js/cytospace/cytoscape-popper.js"></script>

    <?php $this->head() ?>
</head>
<body class="product-page">
<?php $this->beginBody() ?>
<nav class="navbar navbar-rose navbar-transparent navbar-fixed-top navbar-color-on-scroll" color-on-scroll="100">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><span><img src="/images/logo.png"></span></a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php if(Yii::$app->user->isGuest): ?>
                    <li>
                        <a href="javascript:void (0)" data-toggle="modal" data-target="#loginModal">
                            <i class="material-icons">account_circle</i>
                            Вход
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#signupModal">
                            <i class="material-icons">assignment</i>
                            Регистрация
                        </a>
                    </li>
                <?php else:?>
                    <li>
                        <a href="<?= Url::to(['/site/cabinet']) ?>">
                            <i class="material-icons">account_circle</i>
                            <?php
                            $user = User::findOne(Yii::$app->user->id);
                            echo $user->username;
                            ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/site/logout']) ?>" data-method="post">
                            <i class="material-icons">reply</i>
                            Выйти
                        </a>
                    </li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</nav>
<?= $content ?>

<footer class="footer footer-black" style="height: 75px">
    <div class="container">
        <div class="copyright pull-right">
            &copy; 2018 Все права защищены.
        </div>
    </div>
    <div class="loader hidden">

        <img src="/images/loader3.gif"/>
        <div class="loader-div">
            <form name=MyForm>
                <input name="stopwatch" size="10" value="00:00:00.00" class="timer">
            </form>
            <p class="text-primary">Происходит поиск, ожидайте результатов поиска</p>
            <a href="<?= Url::to(['/site/index']) ?>" class="btn btn-primary">Отменить поиск</a>
        </div>

    </div>
    <a id="download" class="hidden" download=""></a>
</footer>

<?php $this->endBody() ?>
</body>
<script type="text/javascript">

    $(document).ready(function () {
        $("#flexiselDemo1").flexisel({
            visibleItems: 4,
            itemsToScroll: 1,
            animationSpeed: 400,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint: 480,
                    visibleItems: 3
                },
                landscape: {
                    changePoint: 640,
                    visibleItems: 3
                },
                tablet: {
                    changePoint: 768,
                    visibleItems: 3
                }
            }
        });
    });
</script>


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Вход</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/site/login'])
                    ]) ?>
                    <div class="card-content">

                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">face</i>
								</span>
                            <div class="form-group is-empty">
                                <?= $form->field($login, 'username')->textInput(['placeholder' => "Логин"])->label(false) ?>
                                <span class="material-input"></span>
                            </div>
                        </div>

                        <div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">lock_outline</i>
								</span>
                            <div class="form-group is-empty">
                                <?= $form->field($login, 'password')->passwordInput(['placeholder' => "Пароль"])->label(false) ?>
                                <span class="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; padding-left: 20px">
                        <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit">Войти</button>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="material-icons">clear</i></button>

                    <div class="header header-primary text-center">
                        <h4 class="card-title">Регистрация</h4>
                    </div>
                    <div class="modal-body">
                        <?php $formSign = ActiveForm::begin([
                            'action' => Url::to(['/site/signup']),
                            'method' => 'post'

                        ]) ?>
                        <div class="card-content">
                            <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">face</i>
										</span>
                                <div class="form-group is-empty">
                                    <?= $formSign->field($signup, 'username')->textInput(['placeholder' => 'Логин'])->label(false) ?>

                                    <span class="material-input"></span></div>
                            </div>

                            <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">email</i>
										</span>
                                <?= $formSign->field($signup, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email'])->label(false) ?>
                                <span class="material-input"></span>
                            </div>
                        </div>

                        <div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">lock_outline</i>
										</span>
                            <div class="form-group is-empty">
                                <?= $formSign->field($signup, 'password')->textInput(['type' => 'password', 'placeholder' => 'Пароль'])->label(false) ?>
                                <span class="material-input"></span></div>
                        </div>
                        <div style="text-align: center; padding-left: 20px">
                            <button class="btn btn-primary btn-simple btn-wd btn-lg" type="submit">Регистрация</button>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</html>
<?php $this->endPage() ?>
