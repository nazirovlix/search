<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 17.01.2019
 * Time: 1:29
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="material-icons">clear</i>
    </button>
    <h4 class="modal-title">Mesh: <?= $article['mesh'] ?></h4>
</div>
<div class="modal-body">
    <p><?= $article['pmids'] ?></p>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Закрыть</button>
</div>
