<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.12.2018
 * Time: 23:15
 */

use yii\helpers\Url;
$this->title = 'Кабинет';
?>

<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
</div>

<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 500px">
            <?php if (!empty($querys)): ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Search Words</th>
                            <th>Result</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($querys)): ?>
                            <?php foreach ($querys as $k => $item): ?>
                                <tr>
                                    <th><?= $k+1 ?></th>
                                    <td><?= $item->query ?></td>
                                    <td><?= $item->countMeshes ?></td>
                                    <th><?= date('d.m.Y H:i',$item->created_at) ?></th>
                                    <th><a class="btn btn-primary" href="<?= Url::to(['/site/user-mesh','id' => $item->id]) ?>">View</a></th>

                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>

            <?php else: ?>
                <h3>Сохраненных результатов нет</h3>
            <?php endif; ?>

            <?php if(Yii::$app->session->get('save')):?>
                <?php $this->registerJs('swal("Успешно", "Ваш поиск успешно сохранено", "success");');
                    Yii::$app->session->destroy();
                 ?>

            <?php endif; ?>
        </div>
    </div>
</div>

