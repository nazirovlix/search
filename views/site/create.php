<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'B.ENGINE';

?>

<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
    <?php if (!empty($articles)): ?>
        <div class="container">
            <div class="row title-row">
                <div class="col-md-3" style="display: flex">
                    <a href="<?= Url::to(['/site/third']) ?>"
                       class="btn btn-white pull-right"><i
                                class="material-icons">keyboard_backspace</i> Назад</a>
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills nav-pills-rose">
                        <li class="<?= $tab == 'tables' ? 'active' : '' ?>"><a href="#tables" data-toggle="tab"
                                                                               aria-expanded="<?= $tab === 'tables' ? 'true' : 'false' ?>">Визуальная
                                карта
                                связей</a>
                        </li>
                        <li class="<?= $tab == 'graph' ? 'active' : '' ?>"><a href="#graph" data-toggle="tab"
                                                                              aria-expanded="<?= $tab === 'graph' ? 'true' : 'false' ?>">Таблица
                                обьектов</a></li>
                        <li class="<?= $tab == 'sortTable' ? 'active' : '' ?>"><a href="#sortTable" data-toggle="tab"
                                                                                  aria-expanded="<?= $tab === 'sortTable' ? 'true' : 'false' ?>">Таблица
                                связей</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-white pull-right">Обработано Mesh: <?= $count ?></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 500px">
            <?php if (!empty($articles)): ?>
                <div class="tab-content">
                    <div class="tab-pane <?= $tab == 'graph' ? 'active' : '' ?>" id="graph">
                        <div class="row">
                            <div class="col-md-7"><h4>Результат поиска: <?= Yii::$app->session['search'] ?> &nbsp;&nbsp;|&nbsp;&nbsp;<?= $pmdis == 'pmids' ? 'Связи на основании текущих PMIDs' : 'Связи на основании родственных PMIDs' ?></h4></div>
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <label style="width: 60%; float: left; padding-top: 10px">
                                    На одной странице:
                                </label>
                                <div style="float: right; width: 30%; margin-top: -10px">

                                    <select class="selectpicker select-limit-create" data-search="<?= $search ?>"
                                            data-style="select-with-transition"
                                            title="<?= $limit ?>" data-pmids="<?= $pmdis ?>" data-size="7" tabindex="-98">
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="300">300</option>
                                        <option value="<?= $count ?>">Показать все</option>

                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="table-responsive">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="all" value="" class="checkAll">
                                            </label>
                                        </div>
                                    </th>
                                    <th style="width: 15%">Mesh&nbsp;<a class="sort-result-mesh"
                                                                        data-sort="<?= $sortFirstMesh ?>"><i
                                                    class="material-icons text-muted">sort_by_alpha</i></a></th>
                                    <th>QTY&nbsp;<a class="sort-result" data-sort="<?= $sortFirst ?>"><i
                                                    class="material-icons text-muted">sort</i></a></th>
                                    <th style="width: 50%">PMIDs</th>
                                    <th style="width: 10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($articles)): ?>
                                <form action="<?= Url::to(['/site/delete-mesh']) ?>" id="saveForm">
                                    <?php foreach ($articles as $k => $article): ?>
                                        <tr>
                                            <th><?= $k + (($page - 1) * $limit) + 1 ?></th>
                                            <td>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="delIds[]"
                                                               value="<?= $k + (($page - 1) * $limit) ?>"
                                                               class="articleCheckbox">
                                                    </label>
                                                </div>
                                            </td>
                                            <th><?= $article['mesh'] ?></th>
                                            <td><?php
                                                $count = substr_count($article['pmids'], ',');
                                                echo $count + 1;
                                                ?>
                                            </td>
                                            <td><?= $article['pmids'] ?></td>
                                            <td><a href="<?= Url::to(['/site/delete-mesh',
                                                    'id' => $k + (($page - 1) * $limit),
                                                    'limit' => $limit,
                                                    'page' => $page]) ?>" class="btn btn-danger btn-simple">Удалить</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                            <div class="table-pagination">
                                <ul class="pagination pagination-info">
                                    <?php if ($totalPages != 0 && $totalPages > 1): ?>
                                        <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                                            <li class="<?= $page == $i ? 'active' : '' ?>">
                                                <a href="<?= Url::to(['/site/create', 'search' => $search, 'page' => $i, 'limit' => $limit]) ?>"><?= $i ?></a>
                                            </li>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <input type="hidden" name="limit" value="<?= $limit ?>">
                            <input type="hidden" name="page" value="<?= $page ?>">
                            <button class="btn btn-danger" type="submit">Удалить Mesh</button>
                            <a href="<?= Url::to(['/site/save']) ?>" class="btn btn-primary">Сохранить</a>
                            <button class="btn btn-info downloadResult" style="float: right">Скачать CSV</button>


                        </div>
                        </form>
                    </div>
                    <div class="tab-pane <?= $tab == 'tables' ? 'active' : '' ?>" id="tables">
                        <h4>Результат поиска: <?= Yii::$app->session['search'] ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?= $pmdis == 'pmids' ? 'Связи на основании текущих PMIDs' : 'Связи на основании родственных PMIDs' ?></h4>
                        <form>
                            <input name="tab" value="tables" type="hidden">
                            <input name="pmdis" value="<?= $pmdis?>" type="hidden">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <h5>Связи Танимото:</h5>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group" style="margin: -14px 0 0 0">
                                                <input type="text" name="countMinTan"
                                                       placeholder="от"
                                                       value="<?= !empty($countTan) ? $countTan : '' ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group" style="margin: -14px 0 0 0">
                                                <input type="text" name="countMinTanTo"
                                                       placeholder="до"
                                                       value="<?= !empty($countTanTo) ? $countTanTo : '' ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <h5>Кол-во обьектов не менее чем:</h5>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group" style="margin: -14px 0 0 0">
                                                <input type="text" name="minCount"
                                                       placeholder="кол-во"
                                                       value="<?= !empty($countMin) ? $countMin : '' ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!----------                    GRAPH            --------------->
                        <div class="graph-container">

                            <?= Yii::$app->view->renderFile('@app/views/site/graph.php', [
                                'allArt' => $allArt,
                                'countTan' => !empty($countTan) ? $countTan : 0,
                                'countTanTo' => !empty($countTanTo) ? $countTanTo : 1,
                            ]) ?>
                        </div>


                        <a href="<?= Url::to(['/site/save']) ?>" class="btn btn-primary">Сохранить</a>
                        <button class="btn btn-info" data-toggle="modal" data-target="#downloadQ" style="float: right">
                            Скачать CSV
                        </button>
                        <!----------                  END GRAPH             --------------->
                    </div>
                    <div class="tab-pane <?= $tab == 'sortTable' ? 'active' : '' ?>" id="sortTable">
                        <div class="row">
                            <div class="col-md-8"><h4>Результат поиска: <?= Yii::$app->session['search'] ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?= $pmdis == 'pmids' ? 'Связи на основании текущих PMIDs' : 'Связи на основании родственных PMIDs' ?></h4></div>
                        </div>
                        <div class="table-responsive" style="overflow: inherit">
                            <form>
                                <input name="pmdis" value="<?= $pmdis?>" type="hidden">
                                <input name="tab" value="sortTable" type="hidden">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <h5>Отсечение Танимото:</h5>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group" style="margin: -14px 0 0 0">
                                                <input type="text" name="countTanimoto"
                                                       placeholder="<?= !empty($countTanimoto) ? $countTanimoto : 'например 0.3' ?>"
                                                       class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <h5>Количество связей:</h5>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group" style="margin: -14px 0 0 0">
                                                <input type="text" name="countMinTanimoto"
                                                       placeholder="<?= !empty($countMinTanimoto) ? $countMinTanimoto : '100' ?>"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="30%">Meshs</th>
                                    <th width="20%">Коэффицент танимото</th>
                                    <th>Общее PMIDs</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($tanimoto)): ?>
                                    <?php $counts = 1; ?>
                                    <?php foreach ($tanimoto as $k => $article): ?>
                                        <?php if($counts <= $countMinTanimoto):?>
                                            <tr>
                                                <th><?= $counts++ ?></th>
                                                <th><?= $article['meshOne'] . ' -- ' . $article['meshTwo'] ?></th>
                                                <td><?= $article['tanimoto'] ?></td>
                                                <td class="td-pmids"><?= implode(', ',$article['pmids']); ?></td>
                                            </tr>
                                        <?php endif;?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                </tbody>
                            </table>
                            <input type="hidden" name="limit" value="<?= $limit ?>">
                            <input type="hidden" name="page" value="<?= $page ?>">
                            <a href="<?= Url::to(['/site/save']) ?>" class="btn btn-primary">Сохранить</a>
                            <button class="btn btn-info downloadTanimoto" style="float: right">Скачать CSV</button>


                        </div>

                    </div>
                </div>
            <?php else: ?>
                <h3>Mash Теги отсутствует</h3>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalPMIDS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content pmid-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                    the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large
                    language ocean. A small river named Duden flows by their place and supplies it with the necessary
                    regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.
                    Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic
                    life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the
                    far World of Grammar.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-simple">Nice Button</button>
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Close
                    <div class="ripple-container">
                        <div class="ripple ripple-on ripple-out"
                             style="left: 48.5938px; top: 17px; background-color: rgb(244, 67, 54); transform: scale(8.51173);"></div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>

<!--// Download Model-->
<div class="modal fade" id="downloadQ" tabindex="-1" role="dialog" aria-labelledby="downloadQ" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">ВЫБЕРИТЕ ТИП ДАННЫХ ДЛЯ ЭКСПРОТА</h4>
            </div>
            <div class="modal-body">
                <form style="margin: 20px">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="meshExport" checked="">
                            MESHs
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pmidExport" checked="">
                            PMID и количество
                        </label>
                    </div>

                </form>
                <button class="btn btn-info downloadResult">Скачать CSV</button>
            </div>

        </div>
    </div>
</div>
