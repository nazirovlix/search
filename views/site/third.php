<?php

/* @var $this yii\web\View */

use app\models\BaseModel;
use yii\helpers\Url;

$this->title = 'B.ENGINE';

?>

<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
    <?php if (!empty($articles)): ?>
        <div class="container">
            <div class="row title-row">
                <div class="col-md-4" style="display: flex">
                    <a href="<?= Url::to(['/site/mesh']) ?>"
                       class="btn btn-white pull-right"><i
                            class="material-icons">keyboard_backspace</i> Назад</a>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <button class="btn btn-white pull-right">Найдено результатов: <?= $count ?></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 500px">
            <?php if (!empty($articles)): ?>
                <div class="row">
                    <div class="col-md-4"><h4>Результат поиска: <?= Yii::$app->session['search'] ?></h4></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label style="width: 60%; float: left; padding-top: 10px">
                            На одной странице:
                        </label>
                        <div style="float: right; width: 30%; margin-top: -10px">
                            <select class="selectpicker select-limit-third" data-search="<?= $search ?>"
                                    data-style="select-with-transition"
                                    title="<?= $limit ?>" data-size="7" tabindex="-98">
                                <option value="100">100</option>
                                <option value="200">200</option>
                                <option value="300">300</option>
                                <option value="<?= $count ?>">Показать все</option>

                            </select>
                        </div>

                    </div>
                </div>
                <div class="table-responsive">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="all" value="" class="checkAll">
                                    </label>
                                </div>
                            </th>
                            <th style="width: 15%">Mesh&nbsp;<a class="sort-result-mesh" data-sort="<?= $sortFirstMesh ?>"><i class="material-icons text-muted">sort_by_alpha</i></a></th>
                            <th>QTY&nbsp;<a class="sort-result" data-sort="<?= $sortFirst ?>"><i class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 10%">Freq. <a class="sort-result" data-sort="<?= $sortFirst ?>"><i class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 10%">PumMed Freq. <a class="sort-feq" data-sort="<?= $sortFirstFeq ?>"><i class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 10%">Million Freq. <a class="sort-feq" data-sort="<?= $sortFirstFeq ?>"><i class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 40%">PMIDs</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($articles)): ?>
                        <form action="<?= Url::to(['/site/delete-mesh-third']) ?>" id="saveForm">
                            <?php foreach ($articles as $k => $article): ?>
                                <tr>
                                    <th><?= $k + (($page - 1) * $limit) + 1 ?></th>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="delIds[]"
                                                       value="<?= $k + (($page - 1) * $limit) ?>"
                                                       class="articleCheckbox">
                                            </label>
                                        </div>
                                    </td>
                                    <th><?= $article['mesh'] ?></th>
                                    <td><?php
                                        $count = substr_count($article['pmids'], ',');
                                        echo ++$count;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $freqAmount = ($count*100)/$amount;
                                        echo round($freqAmount,1) . '%';
                                        ?>
                                    </td>
                                    <td>
                                        <?= round($article['count'],1).'%'; ?>
                                    </td>
                                    <td>
                                        <?= round($article['million'],1).'%'; ?>
                                    </td>
                                    <td class="td-pmids">
                                        <?= $article['pmids'] ?>
                                    </td>
                                    <td><a href="<?= Url::to(['/site/delete-mesh-third',
                                            'id' => $k + (($page - 1) * $limit),
                                            'limit' => $limit,
                                            'page' => $page]) ?>" class="btn btn-danger btn-simple">Удалить</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>

                        </tbody>
                    </table>
                    <div class="table-pagination">
                        <ul class="pagination pagination-info">
                            <?php if ($totalPages != 0 && $totalPages > 1): ?>
                                <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                                    <li class="<?= $page == $i ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/site/third', 'search' => $search, 'page' => $i, 'limit' => $limit]) ?>"><?= $i ?></a>
                                    </li>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <input type="hidden" name="limit" value="<?= $limit ?>">
                    <input type="hidden" name="page" value="<?= $page ?>">
                    <button class="btn btn-danger" type="submit">Удалить Mesh</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Создать связи</button>
                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#downloadResult" style="float: right">Скачать CSV</button>
                </div>
                </form>
            <?php else: ?>
                <h3>Mash Теги отсутствует</h3>
            <?php endif; ?>
        </div>
    </div>
</div>


<!--// Download Model-->
<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="downloadQ" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">ВЫБЕРИТЕ ДАННЫХ ДЛЯ СОЗДАНИЕ СВЯЗИ</h4>
            </div>
            <div class="modal-body">
                <form style="margin: 20px" action="<?= Url::to(['/site/create']) ?>" id="createGraph">
                    <div class="radio">
                        <label>
                            <input type="radio" name="pmdis" value="pmids" required checked="">
                            Создать связи на основании текущих PMIDs
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="pmdis" value="withSimilar" required>
                            Создать связи на основании родственных PMIDs
                        </label>
                    </div>

                <button type="submit" class="btn btn-info">Создать</button>
                </form>
            </div>

        </div>
    </div>
</div>

<!--// Download Model-->
<div class="modal fade" id="downloadResult" tabindex="-1" role="dialog" aria-labelledby="downloadQ" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">ВЫБЕРИТЕ ТИП ДАННЫХ ДЛЯ ЭКСПРОТА</h4>
            </div>
            <div class="modal-body">
                <form style="margin: 20px">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="meshExport" checked="">
                            MESHs
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pmidExport" checked="">
                            PMID и количество
                        </label>
                    </div>

                </form>
                <button class="btn btn-info downloadResult">Скачать CSV</button>
            </div>

        </div>
    </div>
</div>
<div class="load-third loader">

</div>
<input type="hidden" value="<?= Yii::$app->session['amount'] ?: 100 ?>" id="amountModal">