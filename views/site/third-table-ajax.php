<?php

use app\models\BaseModel;
use yii\helpers\Url;

$this->title = 'B.ENGINE';

/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.02.2019
 * Time: 0:51
 */
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="all" value="" class="checkAll">
                </label>
            </div>
        </th>
        <th style="width: 15%">Mesh&nbsp;<a class="sort-result-mesh" data-sort="<?= $sortFirstMesh ?>"><i class="material-icons text-muted">sort_by_alpha</i></a></th>
        <th>QTY&nbsp;<a class="sort-result" data-sort="<?= $sortFirst ?>"><i class="material-icons text-muted">sort</i></a></th>
        <th style="width: 10%">Freq. <a class="sort-result" data-sort="<?= $sortFirst ?>"><i class="material-icons text-muted">sort</i></a></th>
        <th style="width: 10%">PumMed Freq. <a class="sort-feq" data-sort="<?= $sortFirstFeq ?>"><i class="material-icons text-muted">sort</i></a></th>
        <th style="width: 10%">Million Freq. <a class="sort-feq" data-sort="<?= $sortFirstFeq ?>"><i class="material-icons text-muted">sort</i></a></th>
        <th style="width: 40%">PMIDs</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($articles)): ?>
    <form action="<?= Url::to(['/site/delete-mesh-third']) ?>" id="saveForm">
        <?php foreach ($articles as $k => $article): ?>
            <tr>
                <th><?= $k + (($page - 1) * $limit) + 1 ?></th>
                <td>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delIds[]"
                                   value="<?= $k + (($page - 1) * $limit) ?>"
                                   class="articleCheckbox">
                        </label>
                    </div>
                </td>
                <th><?= $article['mesh'] ?></th>
                <td><?php
                    $count = substr_count($article['pmids'], ',');
                    echo ++$count;
                    ?>
                </td>
                <td>
                    <?php
                    $freqAmount = ($count*100)/$amount;
                    echo round($freqAmount,1) . '%';
                    ?>
                </td>
                <td>
                    <?= round($article['count'],1).'%'; ?>
                </td>
                <td>
                    <?= round($article['million'],1).'%'; ?>
                </td>
                <td class="td-pmids"><?= $article['pmids'] ?></td>
                <td><a href="<?= Url::to(['/site/delete-mesh-third',
                        'id' => $k + (($page - 1) * $limit),
                        'limit' => $limit,
                        'page' => $page]) ?>" class="btn btn-danger btn-simple">Удалить</a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php endif; ?>

    </tbody>
</table>
<div class="table-pagination">
    <ul class="pagination pagination-info">
        <?php if ($totalPages != 0 && $totalPages > 1): ?>
            <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                <li class="<?= $page == $i ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/site/third', 'search' => $search, 'page' => $i, 'limit' => $limit]) ?>"><?= $i ?></a>
                </li>
            <?php endfor; ?>
        <?php endif; ?>
    </ul>
</div>
<input type="hidden" name="limit" value="<?= $limit ?>">
<input type="hidden" name="page" value="<?= $page ?>">
<button class="btn btn-danger" type="submit">Удалить Mesh</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Создать связи</button>
<button type="button" class="btn btn-info"  data-toggle="modal" data-target="#downloadResult" style="float: right">Скачать CSV</button>


