<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'B.ENGINE';
?>
<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
    <?php if (!empty($articles)): ?>
        <div class="container">
            <div class="row title-row">
                <div class="col-md-2" style="display: flex">
                    <a href="<?= Url::to(['/site/index', 'search' => Yii::$app->session['search'] ?: '', 'amount' => Yii::$app->session['amount'] ?: 10]) ?>"
                       class="btn btn-white pull-right"><i
                                class="material-icons">keyboard_backspace</i> Назад</a>
                </div>
                <div class="col-md-4 col-md-offset-6">
                    <button class="btn btn-white pull-right">Найдено результатов: <?= $count ?></button>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="container">
            <div class="row title-row">
                <div class="col-md-2" style="display: flex">
                    <a href="/" class="btn btn-white pull-right"><i
                                class="material-icons">keyboard_backspace</i> Назад к поиску</a>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>
<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 500px">
            <?php if (!empty($articles)): ?>
                <div class="row">
                    <div class="col-md-4"><h4>Результат поиска: <?= Yii::$app->session['search'] ?></h4></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label style="width: 60%; float: left; padding-top: 10px">
                            На одной странице:
                        </label>
                        <div style="float: right; width: 30%; margin-top: -10px">
                            <select class="selectpicker select-limit-mesh" data-search="<?= $search ?>"
                                    data-style="select-with-transition"
                                    title="<?= $limit ?>" data-size="7" tabindex="-98">
                                <option value="100">100</option>
                                <option value="200">200</option>
                                <option value="300">300</option>
                                <option value="<?= $count ?>">Показать все</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th style="width: 10%">PMID&nbsp;<a
                                        href="<?= Url::to(['/site/mesh', 'sortID' => $sortFirstID]) ?>"><i
                                            class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 12%">Количество&nbsp;<a
                                        href="<?= Url::to(['/site/mesh', 'sort' => $sortFirst]) ?>"><i
                                            class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 70%">MESHs</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($articles)): ?>
                            <?php foreach ($articles as $k => $article): ?>
                                <tr>
                                    <td class="text-center"><?= $k + (($page - 1) * $limit) + 1 ?></td>

                                    <td><?= $article['pmid'] ?></td>
                                    <td><?= count($article['meshs']) ?></td>
                                    <td>
                                        <?php foreach ($article['meshs'] as $item): ?>
                                            <?= $item . ' <span class="text-primary">|</span> ' ?>
                                        <?php endforeach; ?>
                                    </td>


                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        </tbody>
                    </table>
                    <div class="table-pagination">
                        <ul class="pagination pagination-info">
                            <?php if ($totalPages != 0 && $totalPages > 1): ?>
                                <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                                    <li class="<?= $page == $i ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/site/mesh', 'search' => $search, 'page' => $i, 'limit' => $limit]) ?>"><?= $i ?></a>
                                    </li>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <input type="hidden" name="limit" value="<?= $limit ?>">
                    <input type="hidden" name="page" value="<?= $page ?>">
                    <input type="hidden" name="mesh" value="mesh">
                    <form action="<?= Url::to(['/site/third']) ?>" id="indexSearch" type="get">
                        <button type="submit" class="btn btn-primary thirdSubmit">Обработать mesh и получить статистику</button>
                        <button type="button" class="btn btn-info " data-toggle="modal" data-target="#downloadQ" style="float: right">
                            Скачать CSV
                        </button>
                    </form>

                </div>
            <?php else: ?>
                <h3>Mesh Теги отсутствуют</h3>
            <?php endif; ?>
        </div>
    </div>
</div>

<!--// Download Model-->
<div class="modal fade" id="downloadQ" tabindex="-1" role="dialog" aria-labelledby="downloadQ" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">ВЫБЕРИТЕ ТИП ДАННЫХ ДЛЯ ЭКСПРОТА</h4>
            </div>
            <div class="modal-body">
                <form style="margin: 20px">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pmidExport" checked="">
                            PMID
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="meshExport" checked="">
                            MESHs и количество
                        </label>
                    </div>
                </form>
                <button class="btn btn-info downloadMesh">Скачать CSV</button>
            </div>

        </div>
    </div>
</div>
<input type="hidden" value="<?= Yii::$app->session['amount'] ?: 100 ?>" id="amountModal">