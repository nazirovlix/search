<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Главная';
?>

<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
    <?php if (!empty($articles)): ?>
        <div class="container">
            <div class="row title-row">
                <div class="col-md-2" style="display: flex">
                    <a href="<?= Url::to(['/']) ?>" class="btn btn-white pull-right"><i
                                class="material-icons">keyboard_backspace</i> Назад к поиску</a>
                </div>
                <div class="col-md-4 col-md-offset-6">
                    <button class="btn btn-white pull-right">Найдено
                        результатов: <?= !empty($allCount) ? $allCount : '' ?> <?= '(из них запрошено ' . $count . ' )' ?></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 600px">
            <?php if (empty($articles)): ?>
                <div class="text-center">

                    <h2 class="title">Some Text</h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad assumenda at
                        consequuntur, cupiditate dignissimos esse labore minus nesciunt obcaecati odio officiis
                        pariatur, perferendis temporibus vel. Ipsum iusto nihil soluta.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad assumenda at
                        consequuntur, cupiditate dignissimos esse labore minus nesciunt obcaecati odio officiis
                        pariatur, perferendis temporibus vel. Ipsum iusto nihil soluta.</p>
                    <?php if (!empty($search)): ?>
                        <p class="text-primary" style="margin-top: 20px">По вашему запросу "<?= $search ?>" ничего не
                            найдено. Попробуйте другой вопрос</p>
                    <?php endif; ?>
                    <div class="card card-form-horizontal" style="margin-top: 50px">
                        <div class="card-content">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="input-group">
	        											<span class="input-group-addon">
	        												<i class="material-icons">search</i>
	        											</span>
                                        <div class="form-group is-empty first-search-input">
                                            <input type="text" id="firstSearch" placeholder="Введите слово"
                                                   class="form-control" required>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                    <p class="description">Введите слово или список слов через запятую (Jhon, Jhon
                                        Lennon)</p>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-block btn-search">Искать
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-md-4"><h4>Результат поиска: <?= Yii::$app->request->get('search') ?></h4></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label style="width: 60%; float: left; padding-top: 10px">
                            На одной странице:
                        </label>
                        <div style="float: right; width: 30%; margin-top: -10px">
                            <select class="selectpicker select-limit" data-search="<?= $search ?>"
                                    data-style="select-with-transition" data-amount="<?= $amount ?>"
                                    title="<?= $limit ?>" data-size="7" tabindex="-98">
                                <option value="100">100</option>
                                <option value="200">200</option>
                                <option value="300">300</option>
                                <option value="<?= $count ?>">Показать все</option>

                            </select>
                        </div>

                    </div>
                </div>


                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="all" value="" class="checkAll">
                                    </label>
                                </div>
                            </th>
                            <th>PMID <a href="<?= Url::current(['sort' => $sortFirst ])?>"><i class="material-icons text-muted">sort</i></a></th>
                            <th style="width: 70%">Заголовок</th>
                            <th style="width: 10%;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <form action="<?= Url::to(['/site/delete']) ?>" id="search-form"  class="search-index-ajax">
                            <?php foreach ($articles as $k => $article): ?>
                                <tr>
                                    <td class="text-center"><?= $k + (($page - 1) * $limit) + 1 ?></td>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="delIds[]"
                                                       value="<?= $k + (($page - 1) * $limit) ?>"
                                                       class="articleCheckbox">
                                            </label>
                                        </div>
                                    </td>
                                    <td><?= $article['pmid'] ?></td>
                                    <td>
                                        <a href="http://ncbi.nlm.nih.gov/pubmed/<?= $article['pmid'] ?>"
                                            data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="<?= $article['content'] ?>"><?= $article['title'] ?></a>
                                        <p style="font-size: 12px">
                                            <?= $article['journal'] ? $article['journal'] . '. ' : '' ?>
                                            <?= $article['date'] ? $article['date'] . ', ' : '' ?>
                                            <?= $article['pii'] ? 'pii: ' . $article['pii'] . ' ' : '' ?>
                                            <?= $article['doi'] ? 'doi: ' . $article['doi'] . ' ' : '' ?>
                                        </p>
                                    </td>
                                    <td>
                                        <a href="#"
                                           data-url="<?= Url::to(['/site/delete'])?>"
                                           data-id="<?= $k + (($page - 1) * $limit) ?>"
                                           data-limit="<?= $limit ?>"
                                           data-page="<?= $page ?>"
                                           class="btn btn-danger btn-simple index-delete">
                                            Удалить
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                    <div class="table-pagination">
                        <ul class="pagination pagination-info">
                            <?php if ($totalPages != 0 && $totalPages > 1): ?>
                                <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                                    <li class="<?= $page == $i ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/', 'search' => $search, 'page' => $i, 'limit' => $limit,'amount' => $amount]) ?>"><?= $i ?></a>
                                    </li>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <input type="hidden" name="limit" value="<?= $limit ?>">
                    <input type="hidden" name="page" value="<?= $page ?>">
                    <button class="btn btn-danger btn-delete-ajax" type="button">Удалить PMID</button>
                    <button type="button" class="btn btn-primary goSelected">Получить mesh только по выбранным</button>
                    <a href="<?= Url::to(['/site/mesh']) ?>" class="btn btn-primary">Получить все mesh</a>
                    </form>
                    <button class="btn btn-info" data-toggle="modal" data-target="#downloadQ" style="float: right">Скачать CSV</button>

                </div>
            <?php endif; ?>
        </div>
    </div>
</div>


<div class="modal fade" id="mySearchModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="">
    <div class="modal-dialog modal-signup">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">Поиск</h4>
                </div>
                <div class="modal-body">
                    <h4 id="textPoisk" style="text-align: center;text-transform: uppercase;"></h4>
                    <form method="get" action="<?= Url::to(['/site/index']) ?>" id="indexSearch">
                        <div class="row">
                            <input type="hidden" name="search" id="searchM" placeholder="Введите слово"
                                   class="form-control">
                            <div class="col-sm-2">
                                <div class="input-group" style="text-align: right; width:100%;">
                                    <div class="form-group is-empty">
                                        <h4 style="text-transform: uppercase;">Запросить</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="input-group" style="width:100%;">
                                    <input type="text" name="amount" placeholder="Кол-во результатов" id="amountModal"
                                           class="form-control" required>
                                    <span class="material-input"></span>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group" style="width:100%; margin-top: 12px">
                                    <button type="submit" class="btn btn-primary btn-block">Искать</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--// Download Model-->
<div class="modal fade" id="downloadQ" tabindex="-1" role="dialog" aria-labelledby="downloadQ" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">ВЫБЕРИТЕ ТИП ДАННЫХ ДЛЯ ЭКСПОРОТА</h4>
            </div>
            <div class="modal-body">
                <form style="margin: 20px">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pmidExport" checked="">
                            PMID
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="abstExport" checked="">
                            ЗАГОЛОВКИ И АБСТРАКТЫ
                        </label>
                    </div>
                </form>
                <button class="btn btn-info downloadQuery">Скачать CSV</button>
            </div>

        </div>
    </div>
</div>