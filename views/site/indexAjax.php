<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.04.2019
 * Time: 18:00
 */

use yii\helpers\Url;
 ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="all" value="" class="checkAll">
                </label>
            </div>
        </th>
        <th>PMID <a href="<?= Url::current(['sort' => $sortFirst ])?>"><i class="material-icons text-muted">sort</i></a></th>
        <th style="width: 70%">Заголовок</th>
        <th style="width: 10%;"></th>
    </tr>
    </thead>
    <tbody>
    <form action="<?= Url::to(['/site/delete']) ?>" id="searchFormAjax" class="search-index-ajax">
        <?php foreach ($articles as $k => $article): ?>
            <tr>
                <td class="text-center"><?= $k + (($page - 1) * $limit) + 1 ?></td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delIds[]"
                                   value="<?= $k + (($page - 1) * $limit) ?>"
                                   class="articleCheckbox">
                        </label>
                    </div>
                </td>
                <td><?= $article['pmid'] ?></td>
                <td>
                    <a href="http://ncbi.nlm.nih.gov/pubmed/<?= $article['pmid'] ?>"
                       data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="<?= $article['content'] ?>"><?= $article['title'] ?></a>
                    <p style="font-size: 12px">
                        <?= $article['journal'] ? $article['journal'] . '. ' : '' ?>
                        <?= $article['date'] ? $article['date'] . ', ' : '' ?>
                        <?= $article['pii'] ? 'pii: ' . $article['pii'] . ' ' : '' ?>
                        <?= $article['doi'] ? 'doi: ' . $article['doi'] . ' ' : '' ?>
                    </p>
                </td>
                <td>
                    <a href="#"
                       data-url="<?= Url::to(['/site/delete'])?>"
                       data-id="<?= $k + (($page - 1) * $limit) ?>"
                       data-limit="<?= $limit ?>"
                       data-page="<?= $page ?>"
                       class="btn btn-danger btn-simple index-delete">
                        Удалить
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>

    </tbody>
</table>
<div class="table-pagination">
    <ul class="pagination pagination-info">
        <?php if ($totalPages != 0 && $totalPages > 1): ?>
            <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                <li class="<?= $page == $i ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/', 'search' => $search, 'page' => $i, 'limit' => $limit]) ?>"><?= $i ?></a>
                </li>
            <?php endfor; ?>
        <?php endif; ?>
    </ul>
</div>
<input type="hidden" name="limit" value="<?= $limit ?>">
<input type="hidden" name="page" value="<?= $page ?>">
<button class="btn btn-danger btn-delete-ajax" type="button">Удалить PMID</button>
<button type="button" class="btn btn-primary goSelected">Получить mesh только по выбранным</button>
<a href="<?= Url::to(['/site/mesh']) ?>" class="btn btn-primary">Получить все mesh</a>
</form>
<button class="btn btn-info" data-toggle="modal" data-target="#downloadQ" style="float: right">Скачать CSV</button>

