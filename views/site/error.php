<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
//$this->layout = 'main';
?>
<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
</div>

<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product text-center" style="min-height: 500px">
            <h1>Ошибка 4 0 4</h1>
            <h3>Страница не найдена</h3>
            <a href="/" class="btn btn-primary">Вернуться на главную страницу</a>
        </div>
    </div>
</div>
