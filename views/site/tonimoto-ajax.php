<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="material-icons">clear</i>
    </button>
    <h4 class="modal-title">Mesh 1: <b><?= $mesh1 ?></b>, Mesh 2: <b><?= $mesh2 ?></b></h4>
</div>
<div class="modal-body">
    <p>Коэффициент Танимото: <b><?= $koef ?></b></p>
    <p>PMIDS (<?= count($pmids) ?>):</p>
    <div class="pmids" style="max-height: 320px; overflow-y: auto; overflow-x: hidden;">
        <?php $k = 1; ?>
        <div class="row">
            <div class="col-md-4">
                <?php foreach ($pmids

                as $item): ?>
                <?php if ($k == 11 || $k == 21): ?>
            </div>
            <div class="col-md-4">
                <?php endif; ?>
                <p><?= $k++ . ') ' . $item ?></p>
                <?php endforeach; ?>
            </div>
        </div>

    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Закрыть</button>
</div>
