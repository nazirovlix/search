<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.12.2018
 * Time: 23:25
 */

use yii\helpers\Url;

$this->title = 'B.ENGINE';
?>

<div class="page-header header-filter" data-parallax="true" filter-color="rose"
     style="background-image: url('/images/bg2.jpg');">
    <div class="container">
        <div class="row title-row">
            <div class="col-md-4" style="display: flex">
                <a href="<?= Url::to(['/site/cabinet']) ?>" class="btn btn-white pull-right"><i
                            class="material-icons">keyboard_backspace</i> Назад</a>
            </div>
            <div class="col-md-4">
                <ul class="nav nav-pills nav-pills-rose">
                    <li class="active"><a href="#tables" data-toggle="tab" aria-expanded="true">Визуальная карта</a>
                    </li>
                    <li class=""><a href="#graph" data-toggle="tab" aria-expanded="false">Табличный вид</a></li>
                </ul>
            </div>
            <div class="col-md-4">

            </div>


        </div>
    </div>
</div>

<div class="section section-gray">
    <div class="container">
        <div class="main main-raised main-product" style="min-height: 800px">
            <div class="tab-content">
                <div class="tab-pane" id="graph">
                    <?php if (!empty($mesh)): ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 10%">#</th>
                                    <th style="width: 15%">Mesh</th>
                                    <th style="width: 15%">Кол-во обьектов</th>
                                    <th style="widows: 50%;">PMIDS</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($mesh)): ?>
                                    <?php foreach ($mesh as $k => $item): ?>
                                        <tr>
                                            <th><?= $k + (($page - 1) * 20) + 1 ?></th>
                                            <td><?= $item->mesh ?></td>
                                            <td><?= $item->qty ?></td>
                                            <td><?= $item->pmids ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>


                                </tbody>
                            </table>
                        </div>
                        <div class="table-pagination">
                            <?php
                            echo \yii\widgets\LinkPager::widget([
                                'pagination' => $pages,
                                'options' => ['class' => 'pagination pagination-info'],
                                'prevPageLabel' => '',
                                'nextPageLabel' => '',
                                'activePageCssClass' => 'active',

                            ]); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="tab-pane active" id="tables">
                    <h4>Результат поиска: <?= $search ?></h4>
                    <form>
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <h5>Связи Танимото:</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group" style="margin: -14px 0 0 0">
                                            <input type="text" name="countMinTan"
                                                   placeholder="<?= !empty($countMinTan) ? $countMinTan : 'например 0.3' ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group" style="margin: -14px 0 0 0">
                                            <input type="text" name="countMinTanTo"
                                                   placeholder="<?= !empty($countTanTo) ? $countTanTo : 'до' ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <h5>Кол-во обьектов не менее чем:</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group" style="margin: -14px 0 0 0">
                                            <input type="text" name="minCount"
                                                   placeholder="<?= !empty($count) ? $count : 'кол-во' ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-primary btn-block">Ок</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!----------                    GRAPH            --------------->
                    <div class="graph-container">
                        <?= Yii::$app->view->renderFile('@app/views/site/graph.php',[
                            'allArt' => $allArt,
                            'id' => $id,
                            'countTan' => !empty($countTan) ? $countTan : 0,
                            'countTanTo' => !empty($countTanTo) ? $countTanTo : 1,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalPMIDS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content pmid-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-simple">Nice Button</button>
                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Close
                    <div class="ripple-container">
                        <div class="ripple ripple-on ripple-out"
                             style="left: 48.5938px; top: 17px; background-color: rgb(244, 67, 54); transform: scale(8.51173);"></div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>