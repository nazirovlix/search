<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 09.01.2019
 * Time: 1:27
 */

use app\assets\GraphAsset;
use app\models\BaseModel;

?>
<?php

$base = new BaseModel();
$php = [];
if(empty($id)){
    $id = 0;
}
if (!empty($allArt)) {

    $php = $base->getNodes($allArt,$countTan,$countTanTo);
    $php = $base->getEdges($allArt, $php, $countTan,$countTanTo);
    $tonimoto  = $base->getCoefTonimoto($allArt);

    Yii::$app->session->set('tonimoto', $tonimoto);
    Yii::$app->session->set('graph',$allArt);

}

$js = \yii\helpers\Json::encode($php);

?>


<!-- demo src-->
<style>

    #cya {
        position: absolute;
        left: 0;
        top: 150px;
        bottom: 0;
        right: 17em;
    }

    .tip-link {
        display: block;
        /*background-color: #fff;*/
        color: #fff;
        font-size: 13px;
    }

    .tip-link:hover {
        color: #fff;
        text-decoration: underline;
    }

    @media (max-width: 600px) {
        #cya {
            right: 0;
        }
    }
</style>

<div style="width:100%; height: 600px;">
    <div id="cya" style="width: 100%; height: 600px;">
        <div id="loading" class="loader">
            <img src="/images/loader3.gif"/>
            <div class="loader-div">
                <div class="amount-p">
                    <p class="text-primary" style="text-align: center">Ожидайте, строится график</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var tempArray = <?php echo $js; ?>;
    var queryID = <?php echo $id; ?>;

    var h = function (tag, attrs, children) {
        var el = document.createElement(tag);

        Object.keys(attrs).forEach(function (key) {
            var val = attrs[key];

            el.setAttribute(key, val);
        });

        children.forEach(function (child) {
            el.appendChild(child);
        });

        return el;
    };

    var t = function (text) {
        var el = document.createTextNode(text);

        return el;
    };

    var $ = document.querySelector.bind(document);
    var loading = document.getElementById('loading');
    var cy = cytoscape({

        container: document.getElementById('cya'), // container to render in
        elements: tempArray,
        style: [ // the stylesheet for the graph
            {
                selector: 'node',
                style: {
                    "width": "data(score)",
                    "height": "data(score)",
                    "content": "data(name)",
                    "font-size": "13px",
                    "text-valign": "center",
                    "text-halign": "center",
                    "background-color": "data(color)",
                    "text-outline-color": "data(color)",
                    "text-outline-width": "2px",
                    "color": "#fff",
                    "display": "data(display)",
                    "overlay-padding": "6px",
                    "z-index": "10"
                }
            },

            {
                selector: 'edge',
                style: {
                    'width': 1,
                    'line-color': '#ccc',
                    'target-arrow-color': '#ccc',
                    'target-arrow-shape': 'triangle'
                }
            },
            {
                selector: 'node:selected',
                style: {
                    "border-width": "6px",
                    "border-color": "#AAD8FF",
                    "border-opacity": "0.5"
                }
            }
        ],
        layout: {
            name: 'random'
        }
    });


    var makeTippy = function (node, html) {
        return tippy(node.popperRef(), {
            html: html,
            trigger: 'manual',
            arrow: true,
            placement: 'bottom',
            hideOnClick: false,
            interactive: true
        }).tooltips[0];
    };

    var hideTippy = function (node) {
        var tippy = node.data('tippy');

        if (tippy != null) {
            tippy.hide();
        }
    };

    var hideAllTippies = function () {
        cy.nodes().forEach(hideTippy);
    };

    cy.on('tap', function (e) {
        if (e.target === cy) {
            hideAllTippies();
        }
    });

    cy.on('tap', 'edge', function (e) {
        hideAllTippies();
        var th = $(this);
        var source = th[0].data('source');
        var target = th[0].data('target');
        $.ajax({
            url:'/site/tonimoto-modal',
            type: 'get',
            data: {source:source, target: target},
            success: function (res) {
                $('.pmid-container').html(res);
                $('#myModalPMIDS').modal('show');
            },
            error: function () {
                alert('Ошибка сервера')
            }
        })

    });

    cy.on('zoom pan', function (e) {
        hideAllTippies();
    });

    cy.nodes().forEach(function (n) {
        var g = n.data('name');


        var $links = [
            {
                name: 'GeneCard',
                url: 'http://www.genecards.org/cgi-bin/carddisp.pl?gene=' + g
            },
            {
                name: 'UniProt search',
                url: 'http://www.uniprot.org/uniprot/?query=' + g + '&fil=organism%3A%22Homo+sapiens+%28Human%29+%5B9606%5D%22&sort=score'
            },
            {
                name: 'GeneMANIA',
                url: 'http://genemania.org/search/human/' + g
            }
        ].map(function (link) {
            return h('a', {target: '_blank', href: link.url, 'class': 'tip-link'}, [t(link.name)]);
        });

        var tippy = makeTippy(n, h('div', {}, $links));

        n.data('tippy', tippy);

        n.on('click', function (e) {
            e.preventDefault();
            var id = n.data('id');
            $.ajax({
                url:'/site/pmids-model',
                type: 'get',
                data: {id:id, queryID: queryID},
                success: function (res) {
                    $('.pmid-container').html(res);
                    $('#myModalPMIDS').modal('show');
                },
                error: function () {
                    alert('Ошибка сервера')
                }
            })
        });
        n.on('click', 'edge', function(e){
           alert('df');
        });

    });

    var layout = cy.layout({ name: 'random' });
    layout.pon('layoutstop').then(function( event ){
        loading.classList.add("hidden");
    });
    layout.run();
    loading.classList.add("hidden");
</script>