<?php

//$api = [
//    [
//        'pmid' => 212121,
//        'tags' => ['a', 'b', 'c', 'd']
//    ],
//    [
//        'pmid' => 212122,
//        'tags' => ['d', 'c', 'g', 'h']
//    ],
//    [
//        'pmid' => 212123,
//        'tags' => ['h', 'a', 'f', 'q']
//    ]
//];
//$str = 'abcdefghijklmnopqrstuvwxyz';
//$alpha = str_split($str);
//for ($i = 0; $i < 10000; $i++) {
//    $tags = [];
//    for ($k = 0; $k < 10; $k++) {
//        $tags[] = $alpha[mt_rand(0, count($alpha) - 1)];
//    }
//
//    $api[] = [
//        'pmid' => $i + 4999,
//        'tags' => $tags
//    ];
//}
//
//$result = [
//    'a' => '212121, 212123',
//    'b' => '212121'
//];
//$response = [];
//foreach ($api as $pi) {
//    foreach ($pi['tags'] as $tag) {
//        if (isset($response[$tag])) {
//            $response[$tag] = $response[$tag] . ', ' . $pi['pmid'];
//        } else {
//            $response[$tag] = $pi['pmid'];
//        }
//    }
//}
// session_start();
// echo '<pre>';
// print_r($_SESSION['resp']);
//
//
// die;

error_reporting(E_ALL);

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

// Environment
require(__DIR__ . '/../config/bootstrap.php');
require(__DIR__ . '/../config/helpers.php');

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
