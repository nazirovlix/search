//LOADing
function runLoader() {
    $('.loader').removeClass('hidden');
    // start timer
    if (init == 0) {
        ClearСlock();
        dateObj = new Date();
        StartTIME();
        init = 1;
    } else {
        clearTimeout(clocktimer);
        init = 0;
    }
}

function stopLoader() {
    $('.loader').addClass('hidden');
}

// TIMER
var base = 60;
var clocktimer, dateObj, dh, dm, ds, ms;
var readout = '';
var h = 1, m = 1, tm = 1, s = 0, ts = 0, ms = 0, init = 0;

function ClearСlock() {
    clearTimeout(clocktimer);
    h = 1;
    m = 1;
    tm = 1;
    s = 0;
    ts = 0;
    ms = 0;
    init = 0;
    readout = '00:00:00.00';
    document.MyForm.stopwatch.value = readout;
}

function StartTIME() {
    var cdateObj = new Date();
    var t = (cdateObj.getTime() - dateObj.getTime()) - (s * 1000);
    if (t > 999) {
        s++;
    }
    if (s >= (m * base)) {
        ts = 0;
        m++;
    } else {
        ts = parseInt((ms / 100) + s);
        if (ts >= base) {
            ts = ts - ((m - 1) * base);
        }
    }
    if (m > (h * base)) {
        tm = 1;
        h++;
    } else {
        tm = parseInt((ms / 100) + m);
        if (tm >= base) {
            tm = tm - ((h - 1) * base);
        }
    }
    ms = Math.round(t / 10);
    if (ms > 99) {
        ms = 0;
    }
    if (ms == 0) {
        ms = '00';
    }
    if (ms > 0 && ms <= 9) {
        ms = '0' + ms;
    }
    if (ts > 0) {
        ds = ts;
        if (ts < 10) {
            ds = '0' + ts;
        }
    } else {
        ds = '00';
    }
    dm = tm - 1;
    if (dm > 0) {
        if (dm < 10) {
            dm = '0' + dm;
        }
    } else {
        dm = '00';
    }
    dh = h - 1;
    if (dh > 0) {
        if (dh < 10) {
            dh = '0' + dh;
        }
    } else {
        dh = '00';
    }
    readout = dh + ':' + dm + ':' + ds + '.' + ms;
    document.MyForm.stopwatch.value = readout;
    clocktimer = setTimeout("StartTIME()", 1);
}

// END TIMER

// Delete elements
$(document).on('click', '.delete-elements', function () {
    var id = $(this).data('id');
    var vals = $('#elementId').val();
    $('#elementId').attr('value', vals + id);
    alert($('#elementId').val())

});

// Select limits
$('.select-limit').on('change', function () {
    var search = $(this).data('search');
    var limit = $(this).val();
    var amount = $(this).data('amount');
    window.location.href = '/?search=' + search + '&limit=' + limit + '&amount=' + amount;
});
$('.select-limit-mesh').on('change', function () {
    var search = $(this).data('search');
    var limit = $(this).val();
    window.location.href = '/site/mesh?search=' + search + '&limit=' + limit;
});
$('.select-limit-third').on('change', function () {
    var search = $(this).data('search');
    var limit = $(this).val();
    window.location.href = '/site/third?search=' + search + '&limit=' + limit;
});
$('.select-limit-create').on('change', function () {
    var search = $(this).data('search');
    var pmids = $(this).data('pmids');
    var limit = $(this).val();
    window.location.href = '/site/create?tab=graph&search=' + search + '&limit=' + limit+'&pmdis=' + pmids;
});

$(window).on('load', function () {
   $('.load-third').addClass('hidden');
});

// Sort result AJAX
$(document).on('click','.sort-result', function (e) {
   e.preventDefault();
   var sort = $(this).data('sort');
   $.ajax({
       url:'/site/change-sort-result',
       data:{sort:sort},
       type: 'get',
       success: function (res) {
           $('.table-responsive').html(res)
       },
       error: function (res) {
           alert("Системная ошибка")
       }

   })
});


// Sort result Mesh AJAX
$(document).on('click','.sort-result-mesh', function (e) {
    e.preventDefault();
    var sortMesh = $(this).data('sort');
    $.ajax({
        url:'/site/change-sort-result',
        data:{sortMesh:sortMesh},
        type: 'get',
        success: function (res) {
            $('.table-responsive').html(res)
        },
        error: function (res) {
            alert("Системная ошибка")
        }

    })
});


// Sort result Feqs AJAX
$(document).on('click','.sort-feq', function (e) {
    e.preventDefault();
    var sortFeq = $(this).data('sort');
    $.ajax({
        url:'/site/change-sort-result',
        data:{sortFeq:sortFeq},
        type: 'get',
        success: function (res) {
            $('.table-responsive').html(res)
        },
        error: function (res) {
            alert("Системная ошибка")
        }

    })
});

$(document).on('click','.btn-delete-ajax', function () {
    $('#search-form').submit();
    $('#searchFormAjax').submit();
});
$('#searchFormAjax').submit(function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serialize();

    $.ajax({
        url: url,
        data:data,
        type: 'get',
        success: function (res) {
            $('.table-responsive').html(res)
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

// $('#search-form').on('submit',function (e) {
//     e.preventDefault();
//     var url = $(this).attr('action');
//     var data = $(this).serialize();
//
//     $.ajax({
//         url: url,
//         data:data,
//         type: 'get',
//         success: function (res) {
//             $('.table-responsive').html(res)
//         },
//         error: function () {
//             alert('Ошибка сервера')
//         }
//     })
// })

// Submit Index Search
$(document).ready(function () {
     $('#indexSearch').submit(function () {
        var val = $('#amountModal').val();
        var text = '';
        if (val > 1 && val < 100) {
            text = 'до 100 - 1 минуты'
        } else if (val >= 100 && val < 500) {
            text = 'до 500 - 1-2 минуты'
        } else if (val > 500 && val < 1000) {
            text = 'свыше 500 результатов - 2-5 минут'
        } else if (val > 1000) {
            text = 'свыше 1 тыс. результатов - 5-10 минут'
        }
        $('.amount-p').removeClass('hidden');
        $('.text-count').html(text);
        runLoader();
        return true;
    });
    $('#thirdSubmit').click(function () {
        var val = $('#amountModal').val();
        var text = '';
        if (val > 1 && val < 100) {
            text = 'до 100 - 1 минуты'
        } else if (val >= 100 && val <= 500) {
            text = 'до 500 - 1-2 минуты'
        } else if (val > 500 && val < 1000) {
            text = 'свыше 500 результатов - 2-5 минут'
        } else if (val > 1000) {
            text = 'свыше 1 тыс. результатов - 5-10 минут'
        }
        $('.amount-p').removeClass('hidden');
        $('.text-count').html(text);
        runLoader();
        return true;
    });
    $('#createGraph').submit(function () {
        $('.amount-p').html('Ожидайте, строится график').removeClass('hidden');
        $('.loader-div .btn-primary').addClass('hidden')
        runLoader();
        return true;
    });
});




// submit search btn, open modal
$(document).on('click', '.btn-search', function (e) {
    e.preventDefault()
    var search = $('#firstSearch').val();
    if (search.length > 0) {
        $.ajax({
            url: '/site/count-check',
            data: {search: search},
            type: 'get',
            beforeSend: function () {
                $('.loader').removeClass('hidden');
            },
            success: function (res) {
                $('#mySearchModel').modal('show');
                $('#textPoisk').html('По вашему запросу: "' + search + '" найдено <b>' + res + '</b> результатов');
                $('#searchM').attr('value', search);
            },
            complete: function () {
                $('.loader').addClass('hidden');
            },
            error: function () {
                alert('Ошибка сервера')
            }
        })
    } else {
        $('.first-search-input').addClass('has-error is-focused')
    }
});

// Check all
$(document).on('change', '.checkAll', function (e) {
    e.preventDefault();
    if ($('input[name="all"]').is(':checked')) {
        $('.articleCheckbox').attr('checked', true);
    } else {
        $('.articleCheckbox').attr('checked', false);
    }
});

// Selected articles to Mesh
$(document).on('click', '.goSelected', function (e) {
    e.preventDefault();
    $('#search-form').attr('action', '/site/mesh').submit();
});

// Download Query to CSV | index page
$(document).on('click', '.downloadQuery', function (e) {
    e.preventDefault();
    var pmid = $('input[name="pmidExport"]').is(':checked') ? true : false;
    var abst = $('input[name="abstExport"]').is(':checked') ? true : false;
    $.ajax({
        url: '/site/export-query',
        type: 'get',
        data: {pmid: pmid, abst: abst},
        success: function (res) {
            // console.log(res);
            window.location.href = '/uploads/' + res + '.zip';
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

// Download Result to CSV | create page
$(document).on('click', '.downloadResult', function (e) {
    e.preventDefault();
    var pmid = $('input[name="pmidExport"]').is(':checked') ? true : false;
    var mesh = $('input[name="meshExport"]').is(':checked') ? true : false;
    $.ajax({
        url: '/site/export-result',
        data: {pmid: pmid, mesh: mesh},
        type: 'get',
        success: function (res) {
            window.location.href = '/uploads/' + res + '.zip';
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

// Download Mesh to CSV | create page
$(document).on('click', '.downloadMesh', function (e) {
    var pmid = $('input[name="pmidExport"]').is(':checked') ? true : false;
    var mesh = $('input[name="meshExport"]').is(':checked') ? true : false;
    e.preventDefault();
    $.ajax({
        url: '/site/export-mesh',
        data: {pmid: pmid, mesh: mesh},
        type: 'get',
        success: function (res) {
            window.location.href = '/uploads/' + res + '.zip';
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

// Download Tanimoto to CSV | create page
$(document).on('click', '.downloadTanimoto', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/site/export-tanimoto',
        type: 'get',
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            window.location.href = '/uploads/' + res + '.zip';
        },
        complete: function () {
            stopLoader();
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

$(document).on('click','.index-delete', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var url = $(this).data('url');
    var limit = $(this).data('limit');
    var page = $(this).data('page');
    $.ajax({
        url: url,
        data:{id:id,limit:limit,page:page},
        type: 'get',
        success: function (res) {
           $('.table-responsive').html(res)
        },
        error: function () {
            alert('Ошибка сервера')
        }
    })
});

function stopSearch() {
    ClearСlock();
    window.location.reload(true);
    parent.window.location.reload(true);
}