[ // list of graph elements to start with
  { // node a
    data: {
      id: 'a',
      name: "A"
    },
    position: {
      x: 400.0169597039117,
      y: 300.8210888234145
    }
  },
  { // node b
    data: {
      id: 'b',
      name: "B"
    },
    position: {
      x: 200.0169597039117,
      y: 100.8210888234145
    }
  },
  { // node b
    data: { id: 'c',
      name: "C"
    },
    position: {
      x: 300.0169597039117,
      y: 200.8210888234145
    }
  },
  { // edge ab
    data: { id: 'ab', source: 'a', target: 'b' },
    position: {}
  },
  { // edge bc
    data: { id: 'bc', source: 'b', target: 'c' },
    position: {}
  },
  { // edge bc
    data: { id: 'ac', source: 'a', target: 'c' },
    position: {}
  }
]