<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mesh`.
 * Has foreign keys to the tables:
 *
 * - `query`
 */
class m181212_180209_create_mesh_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('mesh', [
            'id' => $this->primaryKey(),
            'query_id' => $this->integer()->notNull(),
            'mesh' => $this->string()->notNull(),
            'pmids' => $this->text(),
            'qty' => $this->integer(),
        ]);

        // creates index for column `query_id`
        $this->createIndex(
            'idx-mesh-query_id',
            'mesh',
            'query_id'
        );

        // add foreign key for table `query`
        $this->addForeignKey(
            'fk-mesh-query_id',
            'mesh',
            'query_id',
            'query',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `query`
        $this->dropForeignKey(
            'fk-mesh-query_id',
            'mesh'
        );

        // drops index for column `query_id`
        $this->dropIndex(
            'idx-mesh-query_id',
            'mesh'
        );

        $this->dropTable('mesh');
    }
}
