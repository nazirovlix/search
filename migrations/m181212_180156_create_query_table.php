<?php

use yii\db\Migration;

/**
 * Handles the creation of table `query`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m181212_180156_create_query_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('query', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'query' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-query-user_id',
            'query',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-query-user_id',
            'query',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-query-user_id',
            'query'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-query-user_id',
            'query'
        );

        $this->dropTable('query');
    }
}
