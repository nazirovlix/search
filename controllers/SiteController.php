<?php

namespace app\controllers;

use app\models\BaseModel;
use app\models\forms\Login;
use app\models\forms\Signup;
use app\models\Mesh;
use app\models\Pmids;
use app\models\Query;
use app\models\User;
use PubMed\Article;
use PubMed\PubMedId;
use PubMed\Term;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionTest()
    {
        $base = new BaseModel();
        $api = new Term();
        $search = Yii::$app->request->get('search');
        $articles = $api->query($search);
        $allArticles = $base->getArray($articles);
        debug($allArticles,1);
        die();
        //return $articles;
    }

    /** Stage One
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        // Add to SESSION
        Yii::$app->session->open();
        $session = Yii::$app->session;
        $search = '';
        $base = new BaseModel();
        $allArticles = [];
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }

        if (Yii::$app->request->get('search')) {
            $search = Yii::$app->request->get('search');
            $amount = Yii::$app->request->get('amount');
            $page = Yii::$app->request->get('page');
            if (($page > 1) || ($session['search'] == $search && ($session['amount'] == $amount) && $session['deleted'] === false)) {
                $allArticles = $session['query'];
            } else {
                $api = new Term();
                if (empty($amount)) {
                    $amount = 10;
                }

                $session['deleted'] = false;
                $api->setReturnMax($amount); // set max returned articles, defaults to 10
                $articles = $api->query($search);

                // Create array
                $allArticles = $base->getArray($articles);
                if (!empty($session['query'])) {
                    $c = count($allArticles);
                    if ($session['count']) {
                        $c = $session['count'];
                    }
                    $session->destroy();
                    $session->set('count', $c);
                    $session->set('amount', $amount);
                }
                $session->set('amount', $amount);
                $session->set('query', $allArticles);
                $session->set('search', $search);
            }
        }

        $sort = Yii::$app->request->get('sort');
        $sortFirst = 'sortDesc';
        if ($sort) {
            $allArticles = $base->sortPmids($allArticles, $sort);
            $sortFirst = $sort === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }

        // Pagination
        $pagination = $base->pagination(Yii::$app->request->get(), $allArticles);
        $allArticles = array_slice($allArticles, $pagination['offset'], $pagination['limit']);

        return $this->render('index', [
            'articles' => $allArticles,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'search' => $search,
            'amount' => $session['amount'],
            'count' => $pagination['total'],
            'allCount' => $session['count'] ? $session['count'] : '',
            'sortFirst' => $sortFirst
        ]);
    }


    /** Check amounts of Articles
     * @return bool|int|mixed
     */
    public function actionCountCheck()
    {
        $base = new BaseModel();
        $session = Yii::$app->session;
        if (Yii::$app->request->get('search')) {
            $search = Yii::$app->request->get('search');
            $api = new Term();
            $api->setReturnMax(1); // set max returned articles
            $articles = $api->query($search);
            // Create array
            $allArticles = $base->getArray($articles);
            $c = count($allArticles);
            if ($session['count']) {
                $c = $session['count'];
            }
            return $c;
        }
        return false;
    }

    /** Stage One
     * @param $id
     * @return string
     */
    public function actionDelete($id = null)
    {
        $session = Yii::$app->session;
        $base = new BaseModel();
        $search = $session['search'];
        $articles = $session['query'];
        $ids = Yii::$app->request->get('delIds');
        $session['deleted'] = true;
        if ($id == 0) {
            unset($articles[0]);
        }
        if (!empty($id)) {
            unset($articles[$id]);
        }
        if (!empty($ids)) {
            foreach ($ids as $id) {
                if ($id == 0) {
                    unset($articles[0]);
                }
                unset($articles[$id]);
            }
        }

        Yii::$app->session['query'] = array_values($articles);
        $articles = Yii::$app->session['query'];
        $sort = Yii::$app->request->get('sort');
        $sortFirst = 'sortDesc';
        $sortFirstID = 'sortDesc';
        if ($sort) {
            $articles = $base->sortPmids($articles, $sort);
            $sortFirst = $sort === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }
        $pagination = $base->pagination(Yii::$app->request->get(), $articles);

        $allArticles = array_slice($articles, $pagination['offset'], $pagination['limit']);
        if (empty($allArticles) && ($pagination['offset'] - $pagination['limit']) >= 0) {
            $allArticles = array_slice($articles, $pagination['offset'] - $pagination['limit'], $pagination['limit']);
        }

        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }

        return $this->renderAjax('indexAjax', [
            'articles' => $allArticles,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'search' => $search,
            'count' => $pagination['total'],
            'amount' => $session['amount'],
            'allCount' => $session['count'] ? $session['count'] : '',
            'sortFirst' => $sortFirst,
            'sortFirstID' => $sortFirstID
        ]);

    }


    /** Stage Two
     * Article's Meshes
     * @return string
     */
    public function actionMesh()
    {
        $allArticles = [];
        $session = Yii::$app->session;
        $base = new BaseModel();
        $search = $session['search'];
        $ids = Yii::$app->request->get('delIds');
        if ($session['query']) {
            if ($ids) {
                foreach ($session['query'] as $key => $article) {
                    $meshs = $article['meshs'];
                    if ($meshs && in_array($key, $ids)) {
                        $allArticles[] = $article;
                    }
                }

            } else {
                foreach ($session['query'] as $article) {
                    $meshs = $article['meshs'];
                    if ($meshs) {
                        $allArticles[] = $article;
                    }
                }
            }
        }
        if (!empty($allArticles)) {
            $session['query'] = $allArticles;
        }
        $sort = Yii::$app->request->get('sort');
        $sortFirst = 'sortDesc';
        if ($sort) {
            $allArticles = $base->sortMesh($allArticles, $sort);
            $sortFirst = $sort === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }
        $sortId = Yii::$app->request->get('sortID');
        $sortFirstID = 'sortDesc';
        if ($sortId) {
            $allArticles = $base->sortPmids($allArticles, $sortId);
            $sortFirstID = $sortId === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }
        $pagination = $base->pagination(Yii::$app->request->get(), $allArticles);
        $allArticles = array_slice($allArticles, $pagination['offset'], $pagination['limit']);

        return $this->render('mesh', [
            'articles' => $allArticles,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'search' => $search,
            'count' => $pagination['total'],
            'sortFirst' => $sortFirst,
            'sortFirstID' => $sortFirstID
        ]);
    }


    /** Stage Three
     * @return string
     */
    public function actionThird()
    {
        $session = Yii::$app->session;
        $base = new BaseModel();
        if (empty($session['result'])) {
            $result = $base->result($session['query']);
            $session['result'] = $result;
        }
        // for second ferq
        if (empty($session['result'][0]['count'])) {
            $session['result'] = $base->addMeshCounts($session['result']);
        }
        // fot third ferq | with million.txt
        if (empty($session['result'][0]['million'])) {
            $xml = $base->millionCount();
            $result = $session['result'];
            foreach ($result as $key => $item) {
                $res = $base->checkPmid($xml, $item['mesh']);
                $result[$key]['million'] = $res ?: 0;
            }
            $session['result'] = $result;
        }
        $allArt = $session['result'];
        $count = Yii::$app->request->get('minCount');
        if ($count) {
            $attr = [];
            foreach ($allArt as $item) {
                $countPmid = substr_count($item['pmids'], ',') + 1;
                if ($countPmid > $count) {
                    $attr[] = $item;
                }
            }
            $allArt = $attr;

        }
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }
        $sortFirst = 'sortAsc';
        $sortFirstMesh = 'sortDesc';
        $sortFirstFeq = 'sortAsc';
        $pagination = $base->pagination(Yii::$app->request->get(), $session['result']);
        $result = array_slice($session['result'], $pagination['offset'], $pagination['limit']);

        return $this->render('third', [
            'articles' => $result,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'count' => $pagination['total'],
            'search' => $session['search'],
            'allArt' => $allArt,
            'countMin' => $count,
            'sortFirst' => $sortFirst,
            'sortFirstMesh' => $sortFirstMesh,
            'sortFirstFeq' => $sortFirstFeq,
            'totalCount' => $session['count'] ?: 1,
            'amount' => $session['amount'] ?: $session['count'],
        ]);
    }


    /**
     * @return string
     */
    public function actionDeleteMeshThird()
    {
        $id = Yii::$app->request->get('id');
        $ids = Yii::$app->request->get('delIds');
        $session = Yii::$app->session;
        $base = new BaseModel();
        $articles = $session['result'];
        if (!empty($id)) {
            unset($articles[$id]);
        }
        if (!empty($ids)) {
            foreach ($ids as $id) {
                unset($articles[$id]);
            }
        }
        $allArt = $session['result'];
        $count = Yii::$app->request->get('minCount');
        if ($count) {
            $attr = [];
            foreach ($allArt as $item) {
                $countPmid = substr_count($item['pmids'], ',') + 1;
                if ($countPmid > $count) {
                    $attr[] = $item;
                }
            }
            $allArt = $attr;

        }

        $sortFirstFeq = 'sortAsc';
        $sortFirst = 'sortDesc';
        $sortFirstID = 'sortDesc';
        Yii::$app->session['result'] = array_values($articles);
        $articles = Yii::$app->session['result'];
        $pagination = $base->pagination(Yii::$app->request->get(), $articles);

        $allArticles = array_slice($articles, $pagination['offset'], $pagination['limit']);
        if (empty($allArticles) && ($pagination['offset'] - $pagination['limit']) >= 0) {
            $allArticles = array_slice($articles, $pagination['offset'] - $pagination['limit'], $pagination['limit']);
        }
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }
        return $this->render('third', [
            'articles' => $allArticles,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'count' => $pagination['total'],
            'search' => $session['search'],
            'sortFirst' => $sortFirst,
            'allArt' => $allArt,
            'sortFirstMesh' => $sortFirstID,
            'totalCount' => $session['count'] ?: 1,
            'amount' => $session['amount'] ?: 1,
            'countMin' => $count,
            'sortFirstFeq' => $sortFirstFeq,
        ]);
    }

    // Stage Third
    // AJAX change SORT
    public function actionChangeSortResult()
    {
        $session = Yii::$app->session;
        $base = new BaseModel();
        $sort = Yii::$app->request->get('sort');
        $sortMesh = Yii::$app->request->get('sortMesh');
        $sortFeq = Yii::$app->request->get('sortFeq');
        if (empty($session['result'])) {
            $result = $base->result($session['query']);
            $session['result'] = $result;
        }
        $sortFirst = 'sortDesc';
        if ($sort) {
            $session['result'] = $base->sortResult($session['result'], $sort);
            $sortFirst = $sort === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }
        $sortFirstMesh = 'sortDesc';
        if ($sortMesh) {
            $session['result'] = $base->sortResultMesh($session['result'], $sortMesh);
            $sortFirstMesh = $sortMesh === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }
        $sortFirstFeq = 'sortDesc';
        if ($sortFeq) {
            $session['result'] = $base->sortResultFeq($session['result'], $sortFeq);
            $sortFirstFeq = $sortFeq === 'sortDesc' ? 'sortAsc' : 'sortDesc';
        }

        $pagination = $base->pagination(Yii::$app->request->get(), $session['result']);
        $result = array_slice($session['result'], $pagination['offset'], $pagination['limit']);

        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }

        return $this->renderAjax('third-table-ajax', [
            'articles' => $result,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'count' => $pagination['total'],
            'search' => $session['search'],
            'sortFirst' => $sortFirst,
            'sortFirstMesh' => $sortFirstMesh,
            'sortFirstFeq' => $sortFirstFeq,
            'totalCount' => $session['count'] ?: 1,
            'amount' => $session['amount'] ?: 1
        ]);

    }


    /** Stage Four
     * Create Mesh from PMID
     * @return string
     */
    public function actionCreate($pmdis = null)
    {

        $session = Yii::$app->session;
        $base = new BaseModel();
        if (empty($session['result'])) {
            $result = $base->result($session['query']);
            $session['result'] = $result;
        }
        $allArt = $session['result'];
        $tab = Yii::$app->request->get('tab');
        $count = Yii::$app->request->get('minCount');
        $countTan = Yii::$app->request->get('countMinTan');
        $countTanTo = Yii::$app->request->get('countMinTanTo');
        $countTanimoto = Yii::$app->request->get('countTanimoto');
        $countMinTanimoto = Yii::$app->request->get('countMinTanimoto') ?: 100;
        $array = $base->resultWithoutA($session['query']);
        if (!empty($pmdis) && $pmdis == 'withSimilar') {
            $allArt = $base->querySimilars();
            $array = $base->querySimilars(false);
        }
        $tanimoto = $base->arrayTanimoto($array, $countTanimoto);
        $session['tanimoto'] = $tanimoto;
        if ($count) {
            $attr = [];
            foreach ($allArt as $item) {
                $countPmid = substr_count($item['pmids'], ',') + 1;
                if ($countPmid >= $count) {
                    $attr[] = $item;
                }
            }
            $allArt = $attr;

        }
        $sortFirst = 'sortDesc';
        $sortFirstMesh = 'sortDesc';
        $this->layout = 'graphs';
        $pagination = $base->pagination(Yii::$app->request->get(), $session['result']);
        $result = array_slice($session['result'], $pagination['offset'], $pagination['limit']);
        if (!empty($pmdis) && $pmdis == 'withSimilar') {
            $sessionResult = $base->querySimilars();
            $pagination = $base->pagination(Yii::$app->request->get(), $sessionResult);
            $result = array_slice($sessionResult, $pagination['offset'], $pagination['limit']);
        }
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }

        return $this->render('create', [
            'articles' => $result,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'count' => $pagination['total'],
            'search' => $session['search'],
            'allArt' => $allArt,
            'countMin' => $count,
            'sortFirst' => $sortFirst,
            'sortFirstMesh' => $sortFirstMesh,
            'countTan' => $countTan,
            'countTanTo' => $countTanTo,
            'tanimoto' => $tanimoto,
            'tab' => !empty($tab) ? $tab : 'tables',
            'countTanimoto' => $countTanimoto,
            'countMinTanimoto' => $countMinTanimoto,
            'pmdis' => !empty($pmdis) ? $pmdis : 'pmdis'
        ]);
    }


    /** Stage four
     * @return string
     */
    public function actionDeleteMesh()
    {
        $id = Yii::$app->request->get('id');
        $ids = Yii::$app->request->get('delIds');
        $session = Yii::$app->session;
        $base = new BaseModel();
        $articles = $session['result'];
        if (!empty($id)) {
            unset($articles[$id]);
        }
        if (!empty($ids)) {
            foreach ($ids as $id) {
                unset($articles[$id]);
            }
        }
        $allArt = $session['result'];
        $count = Yii::$app->request->get('minCount');
        if ($count) {
            $attr = [];
            foreach ($allArt as $item) {
                $countPmid = substr_count($item['pmids'], ',') + 1;
                if ($countPmid > $count) {
                    $attr[] = $item;
                }
            }
            $allArt = $attr;

        }

        $sortFirst = 'sortDesc';
        $sortFirstID = 'sortDesc';
        Yii::$app->session['result'] = array_values($articles);
        $articles = Yii::$app->session['result'];
        $pagination = $base->pagination(Yii::$app->request->get(), $articles);

        $allArticles = array_slice($articles, $pagination['offset'], $pagination['limit']);
        if (empty($allArticles) && ($pagination['offset'] - $pagination['limit']) >= 0) {
            $allArticles = array_slice($articles, $pagination['offset'] - $pagination['limit'], $pagination['limit']);
        }
        $this->layout = 'graphs';
        return $this->render('create', [
            'articles' => $allArticles,
            'totalPages' => $pagination['totalPages'],
            'limit' => $pagination['limit'],
            'page' => $pagination['page'],
            'count' => $pagination['total'],
            'search' => $session['search'],
            'sortFirst' => $sortFirst,
            'allArt' => $allArt,
            'sortFirstID' => $sortFirstID

        ]);

    }

    /**
     * @return string|Response
     */
    public function actionSave()
    {
        if (Yii::$app->getUser()->isGuest) {
            $session = Yii::$app->session;
            $session['url'] = Url::to(['/site/save']);
            return $this->redirect(Url::to(['/site/login']));
        }
        $base = new BaseModel();
        $session = Yii::$app->session;

        if ($session['result']) {
            $search = $session['search'];
            $query = new Query();
            $query->user_id = Yii::$app->user->id;
            $query->query = !empty($search) ? $search : ' ';
            if ($query->save()) {
                foreach ($session['result'] as $item) {
                    $mesh = new Mesh();
                    $mesh->query_id = $query->id;
                    $mesh->mesh = $item['mesh'];
                    $mesh->pmids = $item['pmids'];
                    $mesh->qty = substr_count($item['pmids'], ',') + 1;
                    $mesh->save();
                }

                $querys = Query::find()->where(['user_id' => Yii::$app->user->id])->all();
                $base->destroy();
                Yii::$app->session->setFlash('save');
                return $this->render('cabinet', [
                    'querys' => $querys
                ]);
            }
        }
        $base->destroy();
        $querys = Query::find()->where(['user_id' => Yii::$app->user->id])->all();
        return $this->render('cabinet', [
            'querys' => $querys
        ]);


    }

    /**
     * @return string
     */
    public function actionCabinet()
    {
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->getId();
            $user = User::find()->where(['id' => $id])->one();
            $this->view->params['username'] = $user->username;
        }
        $querys = Query::find()->where(['user_id' => Yii::$app->user->id])->all();
        return $this->render('cabinet', [
            'querys' => $querys
        ]);

    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionUserMesh($id)
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::to(['/site/login']));
        }
        $page = Yii::$app->request->get('page');
        $search = Query::findOne($id);

        $query = Mesh::find()->where(['query_id' => $id]);
        $allArt = Mesh::find()->where(['query_id' => $id])->asArray()->all();
        $count = Yii::$app->request->get('minCount');
        $countTan = Yii::$app->request->get('countMinTan');
        $countTanTo = Yii::$app->request->get('countMinTanTo');
        if ($count) {
            $attr = [];
            foreach ($allArt as $item) {
                $countPmid = substr_count($item['pmids'], ',') + 1;
                if ($countPmid >= $count) {
                    $attr[] = $item;
                }
            }
            $allArt = $attr;

        }

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 50
        ]);
        $mesh = $query->offset($pages->offset)->limit($pages->limit)->all();
        $this->layout = 'graphs';
        return $this->render('user-mesh', [
            'mesh' => $mesh,
            'allArt' => $allArt,
            'pages' => $pages,
            'id' => $id,
            'page' => !empty($page) ? $page : 1,
            'count' => $count,
            'countTan' => $countTan,
            'countTanTo' => $countTanTo,
            'search' => !empty($search->query) ? $search->query : ''
        ]);

    }

    // Show PMIDS Model when Graph select mesh
    public function actionPmidsModel($id, $queryID = null)
    {
        $session = Yii::$app->session;
        $articles = [];
        if (!Yii::$app->user->isGuest && $queryID) {
            $articles = Mesh::find()->where(['query_id' => $queryID])->asArray()->all();
        } else {
            if ($session['result']) {
                $articles = $session['result'];
            }
        }

        if ($articles) {
            $article = $articles[$id];
            return $this->renderAjax('pmids-ajax', ['article' => $article]);
        }
        return false;
    }

    /**
     *  Open Ajax Modal. Tonimoto
     * @param $source
     * @param $target
     * @return string
     */
    public function actionTonimotoModal($source, $target)
    {
        $session = Yii::$app->session;
        $tonimoto = $session['tonimoto'];
        $koef = round($tonimoto[$source][$target], 4);
        $mesh1 = $session['graph'][$source]['mesh'];
        $mesh2 = $session['graph'][$target]['mesh'];
        $pmid = explode(',', $session['graph'][$source]['pmids']);
        $pmid2 = explode(',', $session['graph'][$target]['pmids']);
        $pmids = array_intersect($pmid, $pmid2);

        return $this->renderAjax('tonimoto-ajax', [
            'koef' => $koef,
            'mesh1' => $mesh1,
            'mesh2' => $mesh2,
            'pmids' => $pmids
        ]);
    }


    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            if (!empty(Yii::$app->session['url'])) {
                return $this->redirect(Yii::$app->session['url']);
            }
            return $this->goBack();
        } else {
            $this->layout = 'login';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string|Response
     */
    public function actionSignup()
    {
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signup()) {
                $login = new Login();
                $login->username = $model->username;
                $login->password = $model->password;
                if ($login->login()) {
                    if (!empty(Yii::$app->session['url'])) {
                        return $this->redirect(Yii::$app->session['url']);
                    }
                    return $this->goBack();
                }
                return $this->goHome();
            }
        }
        $this->layout = 'login';
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    // Export Query to CSV
    public function actionExportQuery()
    {
        $pmid = Yii::$app->request->get('pmid') ?: false;
        $abst = Yii::$app->request->get('abst') ?: false;
        $base = new BaseModel();
        $sessions = Yii::$app->session;
        $name = $base->exportCSV($sessions['query'], $pmid, $abst);
        return $name;
    }

    // Export Mesh to CSV
    public function actionExportMesh()
    {
        $base = new BaseModel();
        $session = Yii::$app->session;
        $allArticles = [];
        foreach ($session['query'] as $article) {
            $meshs = $article['meshs'];
            if ($meshs) {
                $allArticles[] = $article;
            }
        }
        $pmid = Yii::$app->request->get('pmid') ?: false;
        $mesh = Yii::$app->request->get('mesh') ?: false;
        $name = $base->resultMesh($allArticles, $pmid, $mesh);
        return $name;
    }

    // Export Result to CSV
    public function actionExportResult()
    {
        $base = new BaseModel();
        $sessions = Yii::$app->session;
        $result = $base->resultWithoutA($sessions['query']);
        $pmid = Yii::$app->request->get('pmid') ?: false;
        $mesh = Yii::$app->request->get('mesh') ?: false;
        $name = $base->exportResultCSV($result, $pmid, $mesh);
        return $name;
    }


    public function actionExportTanimoto()
    {
        $base = new BaseModel();
        $session = Yii::$app->session;
        $name = $base->exportTanimoto($session['tanimoto']);
        return $name;
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {

        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    public function actionDestroy()
    {
        $base = new BaseModel();
        return $base->destroy();


    }

    public function actionSessions()
    {
        if (Yii::$app->session) {
            $sessions = Yii::$app->session;
            echo 'Search----';
            debug($sessions['search'], 1);

            echo 'Query---';
            debug($sessions['query'], 1);

            echo 'Amount----';
            debug($sessions['amount'], 1);

            echo 'Count---';
            debug($sessions['count'], 1);

            echo 'Result----';
            debug($sessions['result'], 1);

            echo 'Url----';
            debug($sessions['url'], 1);

            echo 'Save----';
            debug($sessions['save'], 1);

            echo 'arrayTanimoto----';
            debug($sessions['tanimoto'], 1);

        }
    }

}
